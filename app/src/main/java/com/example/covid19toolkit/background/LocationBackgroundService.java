package com.example.covid19toolkit.background;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.covid19toolkit.APIQuery.DataAPI;
import com.example.covid19toolkit.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class LocationBackgroundService extends Service {
    public static final long INTERVAL = 60 * 1000; // 60 seconds

    NotificationCompat.Builder buildLocationNotification;


    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    @Override
    public void onCreate() {
        if(mTimer != null) {
            mTimer.cancel();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        mTimer.scheduleAtFixedRate(new LocationDataTask(), 0, INTERVAL);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class LocationDataTask extends TimerTask {

        @Override
        public void run() {
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    getCurrentLocationNameTask locationAsyncTask = new getCurrentLocationNameTask();
                    locationAsyncTask.execute(12.2585,109.0526);
//                    BackgroundService.fetchSummaryData summaryAsyncTask = new BackgroundService.fetchSummaryData();
//                    summaryAsyncTask.execute();
                }
            });
        }
    }

    private class getCurrentLocationNameTask extends AsyncTask<Double,Void,Void> {

        @Override
        protected Void doInBackground(Double... Doubles) {
            String RawLocationData = DataAPI.getLocationInfo(Doubles[0], Doubles[1]);
            if (RawLocationData.length() != 0) {
//                sendBroadcast(new Intent("Countries_Data").putExtra("Data", RawLocationData));

                try {
                    // Get current location data
                    JSONObject jsonObject = new JSONObject(RawLocationData);
                    JSONArray addressComponents = jsonObject.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
                    String countryName = addressComponents.getJSONObject(addressComponents.length()-1).getString("long_name");
                    String provinceName = addressComponents.getJSONObject(addressComponents.length()-2).getString("long_name");

                    //Get sharedPreferences handler
                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("Location", Context.MODE_PRIVATE);

                    // Get last location data
                    String lastLocationCountry = sharedPref.getString(getString(R.string.location_country_key), "NULL");
                    String lastLocationProvince = sharedPref.getString(getString(R.string.location_province_key), "NULL");

                    // First time use SharedPreferences or Last location differs from now
                    if(lastLocationCountry.equals("NULL") || !lastLocationCountry.equals(countryName) || !lastLocationProvince.equals(provinceName)) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(getString(R.string.location_country_key), countryName);
                        editor.putString(getString(R.string.location_province_key), provinceName);
                        editor.apply();

                        if (countryName.equals("Vietnam")) {
                            String RawDiseaseData = DataAPI.getProvinceDisease(provinceName);
                            JSONArray jsonArray = new JSONArray(RawDiseaseData);
                            JSONObject provinceDiseaseData = jsonArray.getJSONObject(0);

                        } else {
                            String RawDiseaseData = DataAPI.getCountryDisease(countryName);
                            JSONArray jsonArray = new JSONArray(RawDiseaseData);
                            JSONObject countryDiseaseData = jsonArray.getJSONObject(0);

                        }
                    }



//                    String RawDiseaseData = DataAPI.getLocationDisease(result);
//                    JSONArray jsonArray = new JSONArray(RawDiseaseData);
//                    result = jsonArray.getJSONObject(0).toString();
//                    Log.d("Location", countryName);
//                    Log.d("Location2", city);

//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putString(getString(R.string.location_country_key), countryName);
//                    editor.putString(getString(R.string.location_province_key), provinceName);
//                    editor.apply();



                } catch (JSONException err) {
                    Log.d("Error", err.toString());
                }

            }
            return null;
        }
    }

//    private class fetchSummaryData extends AsyncTask<Void,Void,Void>{
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            String RawSummaryData = DataAPI.fetchSummaryData();
//            if (RawSummaryData.length() != 0)
//                sendBroadcast(new Intent("Summary_Data").putExtra("Data", RawSummaryData));
//            return null;
//        }
//    }
}

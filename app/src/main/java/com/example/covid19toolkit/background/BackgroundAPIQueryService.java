package com.example.covid19toolkit.background;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.covid19toolkit.APIQuery.DataAPI;
import com.example.covid19toolkit.R;
import com.example.covid19toolkit.model.CountryAPI;
import com.example.covid19toolkit.model.CountryData;
import com.example.covid19toolkit.model.CurrentLocation;
import com.example.covid19toolkit.model.InAppNotification;
import com.example.covid19toolkit.model.NewsModel;
import com.example.covid19toolkit.model.ProvinceAPI;
import com.example.covid19toolkit.model.SummaryAPI;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

public class BackgroundAPIQueryService extends Service {
    public static final long INTERVAL = 60 * 1000;
    Notification foregroundNotification;
    NotificationCompat.Builder worldConfirmedNotification,
            VNConfirmedNotification,
            worldDeathsNotification,
            VNDeathsNotification,
            worldRecoveredNotification,
            VNRecoveredNotification,
            buildLocationNotification,
            newsNotification;
    NotificationManager mNotificationManager;

    String GENERAL_CHANNEL_ID = "General";
    String FOREGROUND_CHANNEL_ID = "Foreground";
    String LOCATION_CHANNEL_ID = "Location";

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    NotificationBroadcastReceiver notificationBroadcastReceiver;

    static String NOTIFICATION_DELETED_ACTION = "NOTIFICATION_DELETED_ACTION";
    static String NOTIFICATION_CLICKED_ACTION = "NOTIFICATION_CLICKED_ACTION";

    @Override
    public void onCreate() {
        pref = this.getSharedPreferences("TempData", 0);
        editor = pref.edit();
        notificationBroadcastReceiver = new NotificationBroadcastReceiver();
        IntentFilter notiIntentFilter = new IntentFilter();
        notiIntentFilter.addAction(NOTIFICATION_CLICKED_ACTION);
        notiIntentFilter.addAction(NOTIFICATION_DELETED_ACTION);
        registerReceiver(notificationBroadcastReceiver, notiIntentFilter);

        try {
            foregroundNotification = new NotificationCompat.Builder(this, FOREGROUND_CHANNEL_ID)
                    .setContentTitle("Covid 19 Survival Kit")
                    .setContentInfo("Thông báo này giúp app có thể chạy ẩn và thông báo khi có tin liên quan đến Covid 19")
                    .setOnlyAlertOnce(true)
                    .setPriority(Notification.PRIORITY_MIN)
                    .build();
            mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            startForeground(1, foregroundNotification);
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                fetchCountriesData countriesAsyncTask = new fetchCountriesData();
                countriesAsyncTask.execute();
                fetchSummaryData summaryAsyncTask = new fetchSummaryData();
                summaryAsyncTask.execute();
                fetchTrendsData trendsAsyncTask = new fetchTrendsData();
                trendsAsyncTask.execute();
                fetchProvincesData provincesAsyncTask = new fetchProvincesData();
                provincesAsyncTask.execute();
                fetchTopNewsData topNewsAsyncTask = new fetchTopNewsData();
                topNewsAsyncTask.execute();
                CurrentLocation currentLocation = new CurrentLocation(getApplicationContext());
                Task<Location> locationTask = currentLocation.getCurrentLocation();
                locationTask.addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            getCurrentLocationNameTask locationAsyncTask = new getCurrentLocationNameTask();
                            locationAsyncTask.execute(location.getLatitude(), location.getLongitude());
                        }
                    }
                });
            }
        }, 0, INTERVAL);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(notificationBroadcastReceiver);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private class fetchCountriesData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String rawCountriesData = DataAPI.fetchCountriesData();
            if (rawCountriesData.length() != 0) {
                sendBroadcast(new Intent("Countries_Data").putExtra("Data", rawCountriesData));
                editor.putString("Countries_Data", rawCountriesData);
                editor.commit();
                VNNotiChecker(rawCountriesData);
            }
            return null;
        }

        private void VNNotiChecker(String rawCountriesData) {
            //Init data
            try {
                String title = getResources().getString(R.string.vietnam);
                String content = "";
                String inAppID = "";
                HashMap<String, CountryData> Countries_Data = new HashMap<String, CountryData>();
                Gson gson = new Gson();
                CountryAPI result = gson.fromJson(rawCountriesData, CountryAPI.class);
                if (result.Items != null) {
                    for (CountryData country : result.Items) {
                        Countries_Data.put(country.countryName, country);
                    }
                }
                //Check Noti
                boolean VNNoti = pref.getBoolean("VN_Noti", true);
                int tempVNConfirmed = pref.getInt("VN_Confirmed", -1);
                int tempVNDeaths = pref.getInt("VN_Deaths", -1);
                int tempVNRecovered = pref.getInt("VN_Recovered", -1);

                if (tempVNConfirmed != Countries_Data.get("Vietnam").confirmed) {
                    if (tempVNConfirmed != -1 && VNNoti) {
                        //Create noti for VN Confirmed changed
                        inAppID = String.format("VN_Confirmed_%d", tempVNConfirmed);
                        content = String.format("%s %d.\n%s %d", getResources().getString(R.string.noti1), Countries_Data.get("Vietnam").confirmed - tempVNConfirmed, getResources().getString(R.string.now), Countries_Data.get("Vietnam").confirmed);
                        VNConfirmedNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                .setSmallIcon(R.drawable.bell_icon)
                                .setContentTitle(title)
                                .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, 5, "VN_Confirmed", Countries_Data.get("Vietnam").confirmed, inAppID, "@VIETNAM"))
                                .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, 5, "VN_Confirmed", Countries_Data.get("Vietnam").confirmed, inAppID, "@VIETNAM"))
                                .setOnlyAlertOnce(true)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(content));
                        createNotification(5, VNConfirmedNotification.build(), inAppID, title, content, "@VIETNAM", "@VIETNAM");
                    }
                    editor.putInt("VN_Confirmed", Countries_Data.get("Vietnam").confirmed);
                }
                if (tempVNDeaths != Countries_Data.get("Vietnam").deaths) {
                    if (tempVNDeaths != -1 && VNNoti) {
                        //Create noti for VN Deaths changed
                        inAppID = String.format("VN_Deaths_%d", tempVNDeaths);
                        content = String.format("%s %d.\n%s: %d", getResources().getString(R.string.noti2), Countries_Data.get("Vietnam").deaths - tempVNDeaths, getResources().getString(R.string.now), Countries_Data.get("Vietnam").deaths);
                        VNDeathsNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                .setSmallIcon(R.drawable.bell_icon)
                                .setContentTitle(title)
                                .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, 6, "VN_Deaths", Countries_Data.get("Vietnam").deaths, inAppID, "@VIETNAM"))
                                .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, 6, "VN_Deaths", Countries_Data.get("Vietnam").deaths, inAppID, "@VIETNAM"))
                                .setOnlyAlertOnce(true)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(content));
                        createNotification(6, VNDeathsNotification.build(), inAppID, title, content, "@VIETNAM", "@VIETNAM");
                    }
                    editor.putInt("VN_Deaths", Countries_Data.get("Vietnam").deaths);
                }
                if (tempVNRecovered != Countries_Data.get("Vietnam").recovered) {
                    if (tempVNRecovered != -1 && VNNoti) {
                        //Create noti for VN Recovered changed
                        inAppID = String.format("VN_Recovered_%d-", tempVNRecovered);
                        content = String.format("%s %d.\n%s: %d", getResources().getString(R.string.noti3), Countries_Data.get("Vietnam").recovered - tempVNRecovered, getResources().getString(R.string.now), Countries_Data.get("Vietnam").recovered);
                        VNRecoveredNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                .setSmallIcon(R.drawable.bell_icon)
                                .setContentTitle(title)
                                .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, 7, "VN_Recovered", Countries_Data.get("Vietnam").recovered, inAppID, "@VIETNAM"))
                                .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, 7, "VN_Recovered", Countries_Data.get("Vietnam").recovered, inAppID, "@VIETNAM"))
                                .setOnlyAlertOnce(true)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(content));
                        createNotification(7, VNRecoveredNotification.build(), inAppID, title, content, "@VIETNAM", "@VIETNAM");
                    }
                    editor.putInt("VN_Recovered", Countries_Data.get("Vietnam").recovered);
                }
                editor.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class fetchSummaryData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String rawSummaryData = DataAPI.fetchSummaryData();
            if (rawSummaryData.length() != 0) {
                sendBroadcast(new Intent("Summary_Data").putExtra("Data", rawSummaryData));
                editor.putString("Summary_Data", rawSummaryData);
                editor.commit();
                summaryNotiChecker(rawSummaryData);
            }
            return null;
        }

        private void summaryNotiChecker(String rawSummaryData) {
            try {
                String title = getResources().getString(R.string.world);
                String content = "";
                String inAppID = "";
                //Init
                HashMap<String, CountryData> Countries_Data = new HashMap<String, CountryData>();
                Gson gson = new Gson();
                SummaryAPI result = gson.fromJson(rawSummaryData, SummaryAPI.class);

                //Check noti
                boolean worldNoti = pref.getBoolean("World_Noti", true);
                int tempWorldConfirmed = pref.getInt("World_Confirmed", -1);
                int tempWorldDeaths = pref.getInt("World_Deaths", -1);
                int tempWorldRecovered = pref.getInt("World_Recovered", -1);
                if (tempWorldConfirmed != result.totalConfirmed) {
                    if (tempWorldConfirmed != -1 && worldNoti) {
                        //Create noti for World confirmed changed
                        inAppID = String.format("World_Confirmed_%d", tempWorldConfirmed);
                        content = String.format("%s %d.\n%s %d", getResources().getString(R.string.noti4), result.totalConfirmed - tempWorldConfirmed, getResources().getString(R.string.now), result.totalConfirmed);
                        worldConfirmedNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                .setSmallIcon(R.drawable.bell_icon)
                                .setContentTitle(title)
                                .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, 2, "World_Confirmed", result.totalConfirmed, inAppID, "@WORLD"))
                                .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, 2, "World_Confirmed", result.totalConfirmed, inAppID, "@WORLD"))
                                .setOnlyAlertOnce(true)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(content));
                        createNotification(2, worldConfirmedNotification.build(), inAppID, title, content, "@WORLD", "@WORLD");
                    } else {
                        editor.putInt("World_Confirmed", result.totalConfirmed);
                    }
                }
                if (tempWorldDeaths != result.totalDeaths) {
                    if (tempWorldDeaths != -1 && worldNoti) {
                        //Create noti for World deaths changed
                        inAppID = String.format("World_Deaths_%d", tempWorldDeaths);
                        content = String.format("%s %d.\n%s %d", getResources().getString(R.string.noti5), result.totalDeaths - tempWorldDeaths, getResources().getString(R.string.now), result.totalDeaths);
                        worldDeathsNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                .setSmallIcon(R.drawable.bell_icon)
                                .setContentTitle(title)
                                .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, 3, "World_Deaths", result.totalDeaths, inAppID, "@WORLD"))
                                .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, 3, "World_Deaths", result.totalDeaths, inAppID, "@WORLD"))
                                .setOnlyAlertOnce(true)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(content));
                        createNotification(3, worldDeathsNotification.build(), inAppID, title, content, "@WORLD", "@WORLD");
                    } else {
                        editor.putInt("World_Deaths", result.totalDeaths);
                    }
                }
                if (tempWorldRecovered != result.totalRecovered) {
                    if (tempWorldRecovered != -1 && worldNoti) {
                        //Create noti for World recovered changed
                        inAppID = String.format("World_Recovered_%d", tempWorldRecovered);
                        content = String.format("%s %d.\n%s %d", getResources().getString(R.string.noti6), result.totalRecovered - tempWorldRecovered, getResources().getString(R.string.now), result.totalRecovered);
                        worldRecoveredNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                .setSmallIcon(R.drawable.bell_icon)
                                .setContentTitle(title)
                                .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, 4, "World_Recovered", result.totalRecovered, inAppID, "@WORLD"))
                                .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, 4, "World_Recovered", result.totalRecovered, inAppID, "@WORLD"))
                                .setOnlyAlertOnce(true)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(content));
                        createNotification(4, worldRecoveredNotification.build(), inAppID, title, content, "@WORLD", "@WORLD");
                    } else {
                        editor.putInt("World_Recovered", result.totalRecovered);
                    }
                }
                editor.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class fetchTrendsData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String rawTrendsData = DataAPI.fetchTrendsData();
            if (rawTrendsData.length() != 0) {
                sendBroadcast(new Intent("Trends_Data").putExtra("Data", rawTrendsData));
                editor.putString("Trends_Data", rawTrendsData);
                editor.commit();
            }
            return null;
        }
    }

    private class fetchProvincesData extends AsyncTask<Void, Void, Void> {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected Void doInBackground(Void... voids) {
            String rawProvincesData = DataAPI.fetchProvincesData();
            if (rawProvincesData.length() != 0) {
                sendBroadcast(new Intent("Provinces_Data").putExtra("Data", rawProvincesData));
                editor.putString("Provinces_Data", rawProvincesData);
                editor.commit();
                provincesNotiChecker(rawProvincesData);
            }
            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        private void provincesNotiChecker(String rawProvincesData) {
            String title = getResources().getString(R.string.vietnam);
            String content = "";
            String inAppID = "";
            try {
                Gson gson = new Gson();
                Type provincesListType = new TypeToken<ArrayList<ProvinceAPI>>() {
                }.getType();
                ArrayList<ProvinceAPI> result = gson.fromJson(rawProvincesData, provincesListType);
                result.sort(ProvinceAPI.compareConfirmed);
                boolean provinces_noti = pref.getBoolean("Provinces_Noti", true);
                for (ProvinceAPI provinceData : result) {
                    int tempProvinceConfirmed = pref.getInt(provinceData.provinceID + "_Confirmed", -1);
                    int tempProvinceDeaths = pref.getInt(provinceData.provinceID + "_Deaths", -1);
                    int tempProvinceRecovered = pref.getInt(provinceData.provinceID + "_Recovered", -1);
                    if (provinceData.confirmed != tempProvinceConfirmed) {
                        if (tempProvinceConfirmed != -1 && provinces_noti) {
                            int notiID = Integer.parseInt(String.format("9%s1", provinceData.provinceID.split("_")[1]));
                            inAppID = String.format("%s_Confirmed_%d", provinceData.provinceID, tempProvinceConfirmed);
                            content = String.format("%s %s $s %d.\n%s %d", getResources().getString(R.string.noti7), provinceData.name, getResources().getString(R.string.increase), provinceData.confirmed - tempProvinceConfirmed, getResources().getString(R.string.now), provinceData.confirmed);
                            VNConfirmedNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                    .setSmallIcon(R.drawable.bell_icon)
                                    .setContentTitle(title)
                                    .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, notiID, provinceData.provinceID + "_Confirmed", provinceData.confirmed, inAppID, "@VIETNAM"))
                                    .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, notiID, provinceData.provinceID + "_Confirmed", provinceData.confirmed, inAppID, "@VIETNAM"))
                                    .setOnlyAlertOnce(true)
                                    .setStyle(new NotificationCompat.BigTextStyle().bigText(content));
                            createNotification(notiID, VNConfirmedNotification.build(), inAppID, title, content, "@VIETNAM", "@VIETNAM");
                        } else {
                            editor.putInt(provinceData.provinceID + "_Confirmed", provinceData.confirmed);
                        }
                    }
                    if (provinceData.deaths != tempProvinceDeaths) {
                        if (tempProvinceDeaths != -1 && provinces_noti) {
                            int notiID = Integer.parseInt(String.format("9%s2", provinceData.provinceID.split("_")[1]));
                            inAppID = String.format("%s_Deaths_%d", provinceData.provinceID, tempProvinceDeaths);
                            content = String.format("%s %s %s %d.\n%s %d", getResources().getString(R.string.noti8), provinceData.name, getResources().getString(R.string.increase), provinceData.deaths - tempProvinceDeaths, getResources().getString(R.string.now), provinceData.deaths);
                            VNDeathsNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                    .setSmallIcon(R.drawable.bell_icon)
                                    .setContentTitle(title)
                                    .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, notiID, provinceData.provinceID + "_Deaths", provinceData.deaths, inAppID, "@VIETNAM"))
                                    .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, notiID, provinceData.provinceID + "_Deaths", provinceData.deaths, inAppID, "@VIETNAM"))
                                    .setOnlyAlertOnce(true)
                                    .setStyle(new NotificationCompat.BigTextStyle().bigText(content));
                            createNotification(notiID, VNDeathsNotification.build(), inAppID, title, content, "@VIETNAM", "@VIETNAM");
                        } else {
                            editor.putInt(provinceData.provinceID + "_Deaths", provinceData.deaths);
                        }
                    }
                    if (provinceData.recovered != tempProvinceRecovered) {
                        if (tempProvinceRecovered != -1 && provinces_noti) {
                            int notiID = Integer.parseInt(String.format("9%s3", provinceData.provinceID.split("_")[1]));
                            inAppID = String.format("%s_Recovered_%d", provinceData.provinceID, tempProvinceRecovered);
                            content = String.format("%s %s %s %d.\n%s %d", getResources().getString(R.string.noti9), provinceData.name, getResources().getString(R.string.increase), provinceData.recovered - tempProvinceRecovered, getResources().getString(R.string.now), provinceData.recovered);
                            VNRecoveredNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                    .setSmallIcon(R.drawable.bell_icon)
                                    .setContentTitle(title)
                                    .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, notiID, provinceData.provinceID + "_Recovered", provinceData.recovered, inAppID, "@VIETNAM"))
                                    .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, notiID, provinceData.provinceID + "_Recovered", provinceData.recovered, inAppID, "@VIETNAM"))
                                    .setOnlyAlertOnce(true)
                                    .setStyle(new NotificationCompat.BigTextStyle().bigText(content));
                            createNotification(notiID, VNRecoveredNotification.build(), inAppID, title, content, "@VIETNAM", "@VIETNAM");
                        } else {
                            editor.putInt(provinceData.provinceID + "_Recovered", provinceData.recovered);
                        }
                    }
                }
                editor.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class fetchTopNewsData extends AsyncTask<Void, Void, Void> {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Void doInBackground(Void... voids) {
            String rawTopNewsData = DataAPI.fetchTopNewsData();
            if (rawTopNewsData.length() != 0) {
                sendBroadcast(new Intent("Top_News_Data").putExtra("Data", rawTopNewsData));
                editor.putString("Top_News_Data", rawTopNewsData);
                editor.commit();
            }
            if (pref.getBoolean("Top_News_Noti", true)) {
                topNewsNotiChecker(rawTopNewsData);
            }
            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        private void topNewsNotiChecker(String rawTopNewsData) {
            try {
                String inAppID = "";
                String[] lastAllNewsID = pref.getString("Top_News_ID", "").split(",");
                JSONObject json = new JSONObject(rawTopNewsData);
                Iterator<?> keys = json.keys();
                ArrayList<String> allNewsID = new ArrayList<>();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    allNewsID.add(key);
//                    Log.w("News", key);
                    if (!isContain(lastAllNewsID, key)) {
                        inAppID = String.format("News_%s", key);
                        Gson gson = new Gson();
                        NewsModel temp = gson.fromJson(json.getJSONObject(key).toString(), NewsModel.class);
                        newsNotification = new NotificationCompat.Builder(getApplicationContext(), GENERAL_CHANNEL_ID)
                                .setSmallIcon(R.drawable.bell_icon)
                                .setLargeIcon(downloadImage(temp.getPicture()))
                                .setContentTitle(temp.getTitle())
                                .setOnlyAlertOnce(true)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(temp.getContent()))
                                .setContentIntent(getIntent(NOTIFICATION_CLICKED_ACTION, key.hashCode(), null, 0, inAppID, temp.getUrl()))
                                .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, key.hashCode(), null, 0, inAppID, temp.getUrl()));
                        createNotification(key.hashCode(), newsNotification.build(), inAppID, temp.getTitle(), temp.getContent(), temp.getPicture(), temp.getUrl());
                    }
                }
                editor.putString("Top_News_ID", String.join(",", allNewsID));
                editor.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private Bitmap downloadImage(String url) {
            try {
                URL link = new URL(url);
                return BitmapFactory.decodeStream(link.openConnection().getInputStream());
            } catch (Exception e) {
                e.printStackTrace();
                return BitmapFactory.decodeResource(getResources(), R.drawable.img_example_new);
            }
        }

        private boolean isContain(String[] array, String data) {
            for (String temp : array) {
                if (data.equals(temp)) return true;
            }
            return false;
        }
    }

    protected PendingIntent getIntent(String action, int id, String prefKey, int newValue, String inAppID, String targetURL) {
        Intent intent = new Intent(action);
        intent.putExtra("Target_URL", targetURL);
        intent.putExtra("Noti_ID", id);
        intent.putExtra("Pref_Key", prefKey);
        intent.putExtra("New_Value", newValue);
        intent.putExtra("In_App_ID", inAppID);
        return PendingIntent.getBroadcast(this, id, intent, 0);
    }

    private class getCurrentLocationNameTask extends AsyncTask<Double, Void, Void> {
        @Override
        protected Void doInBackground(Double... Doubles) {
            String RawLocationData = DataAPI.getLocationInfo(Doubles[0], Doubles[1]);

            if (RawLocationData.length() != 0) {
                try {
                    String inAppID = "";
                    // Get current location data
                    JSONObject jsonObject = new JSONObject(RawLocationData);
                    JSONArray addressComponents = jsonObject.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
                    String countryName = addressComponents.getJSONObject(addressComponents.length() - 1).getString("long_name");
                    String provinceName = addressComponents.getJSONObject(addressComponents.length() - 2).getString("long_name");

                    //Get sharedPreferences handler
                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("Location", Context.MODE_PRIVATE);

                    // Get last location data
                    String lastLocationCountry = sharedPref.getString(getString(R.string.location_country_key), "NULL");
                    String lastLocationProvince = sharedPref.getString(getString(R.string.location_province_key), "NULL");

                    // First time use SharedPreferences or Last location differs from now
                    if (lastLocationCountry.equals("NULL") || !lastLocationCountry.equals(countryName) || !lastLocationProvince.equals(provinceName)) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(getString(R.string.location_country_key), countryName);
                        editor.putString(getString(R.string.location_province_key), provinceName);
                        editor.apply();

                        String result = "Thông tin về dịch bệnh đang bị lỗi";

                        if (countryName.equals("Vietnam")) {
                            String RawDiseaseData = DataAPI.getProvinceDisease(provinceName);
                            JSONArray jsonArray = new JSONArray(RawDiseaseData);
                            JSONObject provinceDiseaseData = jsonArray.getJSONObject(0);
                            int confirmed = provinceDiseaseData.getInt("confirmed"), recovered = provinceDiseaseData.getInt("recovered"), deaths = provinceDiseaseData.getInt("deaths");

                            result = String.format("%s %s, %s\n%s:%d\n%s:%d\n%s:%d", getResources().getString(R.string.noti10), provinceName, getResources().getString(R.string.noti11), getResources().getString(R.string.victim_num), confirmed, getResources().getString(R.string.recover_num), recovered, getResources().getString(R.string.dead_num), deaths);

                        } else {
                            String RawDiseaseData = DataAPI.getCountryDisease(countryName);
                            JSONArray jsonArray = new JSONArray(RawDiseaseData);
                            JSONObject countryDiseaseData = jsonArray.getJSONObject(0);
                            int confirmed = countryDiseaseData.getInt("confirmed"), recovered = countryDiseaseData.getInt("recovered"), deaths = countryDiseaseData.getInt("deaths");

                            result = String.format("%s %s, %s\n%s:%d\n%s:%d\n%s:%d", getResources().getString(R.string.noti10), countryName, getResources().getString(R.string.noti11), getResources().getString(R.string.victim_num), confirmed, getResources().getString(R.string.recover_num), recovered, getResources().getString(R.string.dead_num), deaths);

                        }

                        inAppID = String.format("Location_%d", new Date().getTime());

                        buildLocationNotification = new NotificationCompat.Builder(getApplicationContext(), LOCATION_CHANNEL_ID)
                                .setSmallIcon(R.drawable.bell_icon)
                                .setContentTitle(getResources().getString(R.string.disease))
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(result))
                                .setContentIntent(getIntent(NOTIFICATION_DELETED_ACTION, 8, null, 0, inAppID, "@LOCATION"))
                                .setDeleteIntent(getIntent(NOTIFICATION_DELETED_ACTION, 8, null, 0, inAppID, "@LOCATION"));
                        createNotification(8, buildLocationNotification.build(), inAppID, getResources().getString(R.string.disease), result, "@LOCATION", "@LOCATION");

                    }


//                    String RawDiseaseData = DataAPI.getLocationDisease(result);
//                    JSONArray jsonArray = new JSONArray(RawDiseaseData);
//                    result = jsonArray.getJSONObject(0).toString();
//                    Log.d("Location", countryName);
//                    Log.d("Location2", city);

//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putString(getString(R.string.location_country_key), countryName);
//                    editor.putString(getString(R.string.location_province_key), provinceName);
//                    editor.apply();


                } catch (JSONException err) {
                    Log.d("Error", err.toString());
                }

            }
            return null;
        }
    }

    private void createNotification(int id, Notification notification, String inAppID, String title, String content, String imageURL, String targetURL) {
        //Create noti in app
        InAppNotification inAppNotification = new InAppNotification(inAppID, title, content, imageURL, targetURL);
        SharedPreferences notiPref = this.getSharedPreferences("NotiData", 0);
        SharedPreferences.Editor notiEditor = notiPref.edit();
        notiEditor.putString(inAppID, new Gson().toJson(inAppNotification));
        notiEditor.commit();
        mNotificationManager.notify(id, notification);
    }
}

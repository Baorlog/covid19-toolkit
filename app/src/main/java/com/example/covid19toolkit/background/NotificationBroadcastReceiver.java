package com.example.covid19toolkit.background;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import com.example.covid19toolkit.MainActivity;
import com.example.covid19toolkit.model.InAppNotification;
import com.google.gson.Gson;

public class NotificationBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        {
            if(intent.getAction() == "NOTIFICATION_CLICKED_ACTION"){
                NotificationManager mNotificationManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancel(intent.getExtras().getInt("Noti_ID"));
                Intent startIntent;
                switch(intent.getExtras().getString("Target_URL")) {
                    case "@VIETNAM":
                        startIntent = new Intent(context, MainActivity.class);
                        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(startIntent);
                        break;
                    case "@WORLD":
                        startIntent = new Intent(context, MainActivity.class);
                        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(startIntent);
                        break;
                    case "@LOCATION":
                        startIntent = new Intent(context, MainActivity.class);
                        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(startIntent);
                        break;
                    default:
                        if (intent.getExtras().getString("Target_URL") != null) {
                            startIntent = new Intent(Intent.ACTION_VIEW);
                            startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startIntent.setData(Uri.parse(intent.getExtras().getString("Target_URL")));
                            context.startActivity(startIntent);
                        }
                }
            }
            SharedPreferences pref = context.getSharedPreferences("TempData", 0);
            SharedPreferences.Editor editor = pref.edit();
            SharedPreferences notiPref = context.getSharedPreferences("NotiData", 0);
            SharedPreferences.Editor notiEditor = notiPref.edit();
            try {
                String inAppID = intent.getExtras().getString("In_App_ID");
                Gson gson = new Gson();
                InAppNotification inAppNotification = gson.fromJson(notiPref.getString(inAppID, "ERROR:NOTI_NOT_FOUND"), InAppNotification.class);
                inAppNotification.read();
                notiEditor.putString(inAppID, gson.toJson(inAppNotification));
                notiEditor.commit();
            } catch (Exception e){
                e.printStackTrace();
            }

            if(intent.getExtras().getString("Pref_Key")!=null) {
                editor.putInt(intent.getExtras().getString("Pref_Key"), intent.getExtras().getInt("New_Value"));
            }
            editor.commit();
        }
    }
}

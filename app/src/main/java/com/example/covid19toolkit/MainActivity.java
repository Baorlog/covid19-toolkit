package com.example.covid19toolkit;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ConfigurationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.covid19toolkit.APIQuery.DataAPI;
import com.example.covid19toolkit.adapter.AdapterNotification;
import com.example.covid19toolkit.adapter.EndlessRecyclerViewScrollListener;
import com.example.covid19toolkit.background.BackgroundAPIQueryService;
// import com.example.covid19toolkit.background.BackgroundService;
import com.example.covid19toolkit.model.InAppNotification;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    Intent intentService;
    private AppBarConfiguration mAppBarConfiguration;
    TextView smsCountTxt;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    int pendingSMSCount = 10;
//    private ImageView bellIcon;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

//    Location currentLocation;
//    FusedLocationProviderClient mFusedLocationProviderClient;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION=101; //REQUEST CODE
    boolean mLocationPermissionGranted=false;
//    private BottomNavigationView bottomNavigationView;

    private ArrayList<InAppNotification> notis = new ArrayList<InAppNotification>();

    DrawerLayout drawer;
    Menu menuOptions;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.colorAccent));
        super.onCreate(savedInstanceState);
        pref = this.getSharedPreferences("TempData", 0);
        editor = pref.edit();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        if (!mLocationPermissionGranted){
            getLocationPermission();
        }

//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_map, R.id.nav_news, R.id.nav_statistic)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//                Integer id=menuItem.getItemId();
//                Log.d("Notifi", id.toString());
//                LinearLayout lnNotis= findViewById(R.id.ln_notis);
//                if(lnNotis.getVisibility()==View.VISIBLE) {
//                    Log.d("Notifi", "arghhhh");
//                    lnNotis.setVisibility(View.GONE);
//                }
//                //This is for maintaining the behavior of the Navigation view
//                DrawerLayout drawer = findViewById(R.id.drawer_layout);
//                NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//                NavigationUI.onNavDestinationSelected(menuItem, navController);
//                //This is for closing the drawer after acting on it
//                drawer.closeDrawer(GravityCompat.START);
//                return true;
//            }
//        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeNotiAndSetting();
                drawer.openDrawer(GravityCompat.START);
            }
        });

        //recycleview for notifications
        recyclerView = (RecyclerView) findViewById(R.id.notis_display);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

//        //Tải thêm phần tử danh sách khi cuộn tới cuối
//        EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener
//                = new EndlessRecyclerViewScrollListener(layoutManager) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        loadNextDataFromApi(page);
//                    }
//                }, 1500);
//
//            }
//        };
//        //Thêm Listener vào
//        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

        // specify an adapter (see also next example)
        notis = new ArrayList<>();
        SharedPreferences notiPref = this.getSharedPreferences("NotiData", 0);
        SharedPreferences.Editor notiEditor = notiPref.edit();
        HashMap<String, String> notiList = (HashMap<String, String>) notiPref.getAll();
        for(String inAppID : notiList.keySet()){
            notis.add(new Gson().fromJson(notiList.get(inAppID), InAppNotification.class));
        }
        notis.sort(InAppNotification.compareTime);
        mAdapter = new AdapterNotification(notis, this);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);

        intentService = new Intent(this, BackgroundAPIQueryService.class);
        startService(intentService);

//        intentService = new Intent(this, LocationBackgroundService.class);
//        startService(intentService);

//        String somestr = DataAPI.getLocationInfo(-11.202692,17.873887);
//        Toast.makeText(getApplicationContext(), somestr, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem notiItem = menu.findItem(R.id.notis);

        menuOptions = menu;

        View actionView = MenuItemCompat.getActionView(notiItem);
//        smsCountTxt = (TextView) actionView.findViewById(R.id.notification_badge);
//        bellIcon= (ImageView) actionView.findViewById(R.id.iv_notis_icon);
//
//        setupBadge();

//        actionView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onOptionsItemSelected(notiItem);
//            }
//        });
        return true;
    }

    private void setupBadge() {

        if (smsCountTxt != null) {
            if (pendingSMSCount == 0) {
                if (smsCountTxt.getVisibility() != View.GONE) {
                    smsCountTxt.setVisibility(View.GONE);
                }
            } else {
                smsCountTxt.setText(String.valueOf(Math.min(pendingSMSCount, 99)));
                if (smsCountTxt.getVisibility() != View.VISIBLE) {
                    smsCountTxt.setVisibility(View.VISIBLE);
                }
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notis:
//                item.setIcon(R.drawable.bell_active_icon);
                displayNotis(item);
                return true;

            case R.id.setting:
//                item.setIcon(R.drawable.ic_active_setting);
                setting(item);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }


    private void setting(MenuItem item) {
        LinearLayout lnSetting= findViewById(R.id.ln_setting);
        Switch swWorld = (Switch) findViewById(R.id.sw_world);
        Switch swVN = (Switch) findViewById(R.id.sw_country);
        Switch swProvice = (Switch) findViewById(R.id.sw_province);
        Switch swNews = (Switch) findViewById(R.id.sw_news);
        Switch swEnglish = (Switch) findViewById(R.id.sw_English);
        Switch swVietnamese = (Switch) findViewById(R.id.sw_Vietnamese);
        LinearLayout lnNotis= findViewById(R.id.ln_notis);

        Boolean world_noti = pref.getBoolean("World_Noti", true);
        Boolean VN_noti = pref.getBoolean("VN_Noti", true);
        Boolean provinces_noti = pref.getBoolean("Provinces_Noti", true);
        Boolean top_news_noti = pref.getBoolean("Top_News_Noti", true);

        swWorld.setChecked(world_noti);
        swVN.setChecked(VN_noti);
        swProvice.setChecked(provinces_noti);
        swNews.setChecked(top_news_noti);

        Locale current = getResources().getConfiguration().locale;
        if (current.toString().equalsIgnoreCase("vi")) {
            swVietnamese.setChecked(true);
            swEnglish.setChecked(false);
        } else {
            swEnglish.setChecked(true);
            swVietnamese.setChecked(false);
        }

        if(lnSetting.getVisibility()==View.GONE) {

            lnSetting.setVisibility(View.VISIBLE);
            lnNotis.setVisibility(View.GONE);
            item.setIcon(R.drawable.ic_active_setting);

            MenuItem itemBell = menuOptions.findItem(R.id.notis);
            itemBell.setIcon(R.drawable.bell_icon);

            swWorld.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        editor.putBoolean("World_Noti", true);
                    }
                    else{
                        editor.putBoolean("World_Noti", false);
                    }
                    editor.commit();
                }
            });
            swVN.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        editor.putBoolean("VN_Noti", true);
                    }
                    else{
                        editor.putBoolean("VN_Noti", false);
                    }
                    editor.commit();
                }
            });
            swProvice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        editor.putBoolean("Provinces_Noti", true);
                    }
                    else{
                        editor.putBoolean("Provinces_Noti", false);
                    }
                    editor.commit();
                }
            });
            swNews.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        editor.putBoolean("Top_News_Noti", true);
                    }
                    else{
                        editor.putBoolean("Top_News_Noti", false);
                    }
                    editor.commit();
                }
            });
            swEnglish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        setLocale("en");
                        Intent intent = getIntent();
                        finish();
                        stopService(intentService);
                        startActivity(intent);


                    }
                    else{
                        setLocale("vi");
                        Intent intent = getIntent();
                        finish();
                        stopService(intentService);
                        startActivity(intent);

                    }
                }
            });
            swVietnamese.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        setLocale("vi");
                        Intent intent = getIntent();
                        finish();
                        stopService(intentService);
                        startActivity(intent);

                    }
                    else{
                        setLocale("en");
                        Intent intent = getIntent();
                        finish();
                        stopService(intentService);
                        startActivity(intent);

                    }

                }
            });




        }
        else {lnSetting.setVisibility(View.GONE);
            item.setIcon(R.drawable.ic_inactive_setting);}
    }

    private void setLocale(String lang){
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_lang", lang);
        editor.apply();


    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void displayNotis(MenuItem item) {
        LinearLayout lnNotis= findViewById(R.id.ln_notis);
        LinearLayout lnSetting= findViewById(R.id.ln_setting);
        if(lnNotis.getVisibility()==View.GONE) {
            notis = new ArrayList<>();
            SharedPreferences notiPref = this.getSharedPreferences("NotiData", 0);
            SharedPreferences.Editor notiEditor = notiPref.edit();
            HashMap<String, String> notiList = (HashMap<String, String>) notiPref.getAll();
            for(String inAppID : notiList.keySet()){
                notis.add(new Gson().fromJson(notiList.get(inAppID), InAppNotification.class));
            }
            notis.sort(InAppNotification.compareTime);
            mAdapter = new AdapterNotification(notis, this);
            mAdapter.notifyDataSetChanged();
            recyclerView.setAdapter(mAdapter);
            lnNotis.setVisibility(View.VISIBLE);
            lnSetting.setVisibility(View.GONE);
            item.setIcon(R.drawable.bell_active_icon);

            MenuItem itemSetting = menuOptions.findItem(R.id.setting);
            itemSetting.setIcon(R.drawable.ic_inactive_setting);
        }
        else {
            lnNotis.setVisibility(View.GONE);
            item.setIcon(R.drawable.bell_icon);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }

    }
    public void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()`
    }

    public void closeNotiAndSetting() {
        LinearLayout lnNotis= findViewById(R.id.ln_notis);
        LinearLayout lnSetting= findViewById(R.id.ln_setting);

        if(lnNotis.getVisibility()==View.VISIBLE) {
            lnNotis.setVisibility(View.GONE);
            MenuItem item = menuOptions.findItem(R.id.notis);
            item.setIcon(R.drawable.bell_icon);
        }
        if(lnSetting.getVisibility()==View.VISIBLE) {
            lnSetting.setVisibility(View.GONE);
            MenuItem item = menuOptions.findItem(R.id.setting);
            item.setIcon(R.drawable.ic_inactive_setting);
        }
    }
}

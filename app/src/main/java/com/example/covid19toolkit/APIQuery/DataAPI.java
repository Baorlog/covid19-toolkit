package com.example.covid19toolkit.APIQuery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class DataAPI {

    public static String fetchCountriesData() {
        URL countryEndpoint = null;
        try {
            countryEndpoint = new URL("https://bhxighedt9.execute-api.ap-east-1.amazonaws.com/beta/get-countries");
            HttpsURLConnection myConnection = null;
            try {
                myConnection = (HttpsURLConnection) countryEndpoint.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (myConnection.getResponseCode() == 200)
                    return slurp(myConnection.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String fetchSummaryData() {
        URL countryEndpoint = null;
        try {
            countryEndpoint = new URL("https://bhxighedt9.execute-api.ap-east-1.amazonaws.com/beta/get-summary");
            HttpsURLConnection myConnection = null;
            try {
                myConnection = (HttpsURLConnection) countryEndpoint.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (myConnection.getResponseCode() == 200)
                    return slurp(myConnection.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String fetchTrendsData() {
        URL countryEndpoint = null;
        try {
            countryEndpoint = new URL("https://bhxighedt9.execute-api.ap-east-1.amazonaws.com/beta/get-trends");
            HttpsURLConnection myConnection = null;
            try {
                myConnection = (HttpsURLConnection) countryEndpoint.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (myConnection.getResponseCode() == 200)
                    return slurp(myConnection.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String fetchProvincesData() {
        URL countryEndpoint = null;
        try {
            countryEndpoint = new URL("https://bhxighedt9.execute-api.ap-east-1.amazonaws.com/beta/get-provinces");
            HttpsURLConnection myConnection = null;
            try {
                myConnection = (HttpsURLConnection) countryEndpoint.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (myConnection.getResponseCode() == 200)
                    return slurp(myConnection.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String fetchTopNewsData(){
        URL countryEndpoint = null;
        try {
            countryEndpoint = new URL("https://bhxighedt9.execute-api.ap-east-1.amazonaws.com/beta/get-top-news");
            HttpsURLConnection myConnection = null;
            try {
                myConnection = (HttpsURLConnection) countryEndpoint.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (myConnection.getResponseCode() == 200)
                    return slurp(myConnection.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

    protected static String slurp(InputStream is){
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = "";
        while (true) {
            try {
                if (!((line = br.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            sb.append(line+"\n");
        }
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static String getLocationInfo(double lat, double lng) {
        URL mapEndpoint = null;
        try {
            mapEndpoint = new URL("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=AIzaSyCgfCutwhPh7rbVrqiudUj27lwYQAwm3Ao&language=en");
            HttpsURLConnection myConnection = null;
            try {
                myConnection = (HttpsURLConnection) mapEndpoint.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (myConnection != null && myConnection.getResponseCode() == 200) {
                    return slurp(myConnection.getInputStream());
                } else {
                    return "Unknown coordinates";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getCountryDisease(String countryName) {
        URL mapEndpoint = null;
        try {
            mapEndpoint = new URL("https://bhxighedt9.execute-api.ap-east-1.amazonaws.com/beta/get-countries?countryName=" + countryName);
            HttpsURLConnection myConnection = null;
            try {
                myConnection = (HttpsURLConnection) mapEndpoint.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (myConnection != null && myConnection.getResponseCode() == 200) {
                    return slurp(myConnection.getInputStream());
                } else {
                    return "Unknown location";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getProvinceDisease(String provinceName) {
        URL mapEndpoint = null;
        try {
            mapEndpoint = new URL("https://bhxighedt9.execute-api.ap-east-1.amazonaws.com/beta/get-provinces?provinceName=" + provinceName);
            HttpsURLConnection myConnection = null;
            try {
                myConnection = (HttpsURLConnection) mapEndpoint.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (myConnection != null && myConnection.getResponseCode() == 200) {
                    return slurp(myConnection.getInputStream());
                } else {
                    return "Unknown location";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

}

package com.example.covid19toolkit.model;

public class NewsModel {
    private String picture;
    private String title;
    private String content;
    private String author;
    private String publishDate;
    private String url;
    private String siteName;

    public NewsModel(String picture, String title, String content, String author, String publishDate, String url, String siteName) {
        this.picture = picture;
        this.title = title;
        this.content = content;
        this.author = author;
        this.publishDate = publishDate;
        this.url = url;
        this.siteName = siteName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }
}
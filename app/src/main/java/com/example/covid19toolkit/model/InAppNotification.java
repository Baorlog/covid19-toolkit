package com.example.covid19toolkit.model;

import java.util.Comparator;
import java.util.Date;

public class InAppNotification {
    private String id, title, content;
    private Long createdTime;
    private Boolean isRead;
    private String imageURL;

    private String targetURL;

    public InAppNotification(String id, String title, String content, String imageURL, String targetURL, Long createdTime, Boolean isRead) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.imageURL = imageURL;
        this.targetURL = targetURL;
        this.createdTime = createdTime;
        this.isRead = isRead;
    }

    public InAppNotification(String id, String title, String content, String imageURL,String targetURL, Long createdTime) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.imageURL = imageURL;
        this.targetURL = targetURL;
        this.createdTime = createdTime;
        this.isRead = false;
    }

    public InAppNotification(String id, String title, String content, String imageURL, String targetURL) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.imageURL = imageURL;
        this.targetURL = targetURL;
        this.createdTime = new Date().getTime();
        this.isRead = false;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getImageURL() { 
        return imageURL; 
    }

    public String getTargetURL() {
        return targetURL;
    }


    public Long getCreatedTime() {
        return createdTime;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void read() {
        isRead = true;
    }

    public String compareNow(){
        String result = "";
        Long diff = new Date().getTime() - this.createdTime;
        if(diff<60000){
            return "Vừa xong";
        }
        else if(diff<60000*60){
            return String.format("%d phút trước", (int) Math.floor(diff/(60000)));
        }
        else if(diff<60000*60*24){
            return String.format("%d giờ trước", (int) Math.floor(diff/(60*60000)));
        }
        else {
            return String.format("%d ngày trước", (int) Math.floor(diff/(24*60*60000)));
        }
    }

    public static Comparator<InAppNotification> compareTime= new Comparator<InAppNotification>() {
        public int compare(InAppNotification s1, InAppNotification s2) {
            return (int) ((s2.createdTime)-(s1.createdTime));
        };
    };
}

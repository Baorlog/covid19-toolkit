package com.example.covid19toolkit.model;

public class Country {
    private String country;
    private int victimNumber;
    private int deadNumber;
    private int recoverNumber;

    public Country(){}
    public Country(String addr, int num1, int num2, int num3){
        this.country =addr;
        this.victimNumber=num1;
        this.deadNumber=num2;
        this.recoverNumber=num3;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getVictimNumber() {
        return victimNumber;
    }

    public void setVictimNumber(int distance) {
        this.victimNumber = distance;
    }

    public int getDeadNumber() {
        return deadNumber;
    }

    public void setDeadNumber(int deadNumber) {
        this.deadNumber = deadNumber;
    }

    public int getRecoverNumber() {
        return recoverNumber;
    }

    public void setRecoverNumber(int recoverNumber) {
        this.recoverNumber = recoverNumber;
    }
}

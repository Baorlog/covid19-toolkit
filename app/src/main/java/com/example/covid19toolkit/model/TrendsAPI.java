package com.example.covid19toolkit.model;

import java.util.Comparator;

public class TrendsAPI {
    public int time,
            recovered,
            confirmed,
            deaths;

    public TrendsAPI(int time, int recovered, int confirmed, int deaths) {
        this.time = time;
        this.recovered = recovered;
        this.confirmed = confirmed;
        this.deaths = deaths;
    }

    public static Comparator<TrendsAPI> compareTime = new Comparator<TrendsAPI>() {
        public int compare(TrendsAPI s1, TrendsAPI s2) {
            return (s1.time)-(s2.time);
        };
    };
}

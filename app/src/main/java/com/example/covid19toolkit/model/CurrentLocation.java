package com.example.covid19toolkit.model;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class CurrentLocation {
    private Context context;
    Location currentLocation;
    FusedLocationProviderClient mFusedLocationProviderClient;
    boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION=101; //REQUEST CODE


    public CurrentLocation(Context context){
        this.context = context;
    }

    public Task<Location> getCurrentLocation() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        try {

                return mFusedLocationProviderClient.getLastLocation();

        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
        return null;
    }
}

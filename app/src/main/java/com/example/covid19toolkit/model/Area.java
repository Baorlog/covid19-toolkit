package com.example.covid19toolkit.model;

public class Area {
    private String addr;
    private int victimNumber;

    public Area(){}
    public Area(String addr, int num){
        this.addr=addr;
        this.victimNumber=num;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public int getVictimNumber() {
        return victimNumber;
    }

    public void setVictimNumber(int number) {
        this.victimNumber = number;
    }
}

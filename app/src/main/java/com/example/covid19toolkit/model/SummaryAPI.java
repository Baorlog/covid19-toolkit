package com.example.covid19toolkit.model;

public class SummaryAPI {
    public int totalRecovered,
            totalRecoveredLast,
            totalDeathsLast,
            totalDeaths,
            totalConfirmed,
            totalConfirmedLast;
    public String lastUpdate;

    public SummaryAPI(int totalConfirmed, int totalConfirmedLast, int totalDeaths, int totalDeathsLast, int totalRecovered, int totalRecoveredLast, String lastUpdate){
        this.totalConfirmed = totalConfirmed;
        this.totalConfirmedLast = totalConfirmedLast;
        this.totalDeaths = totalDeaths;
        this.totalDeathsLast = totalDeathsLast;
        this.totalRecovered = totalRecovered;
        this.totalRecoveredLast = totalRecoveredLast;
        this.lastUpdate = lastUpdate;
    }
}

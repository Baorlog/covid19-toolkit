package com.example.covid19toolkit.model;


import java.util.Comparator;

public class CountryData {
    public int confirmed;
    public int deaths;
    public int recovered;
    public String countryName;
    public float map_lat;
    public float map_long;
    public String lastUpdate;

    public CountryData(String countryName, int confirmed, int deaths, int recovered, float map_lat, float map_long, String lastUpdate){
        this.countryName = countryName;
        this.confirmed = confirmed;
        this.deaths = deaths;
        this.recovered = recovered;
        this.map_lat = map_lat;
        this.map_long = map_long;
        this.lastUpdate = lastUpdate;
    }

    public Country toCountry(){
        return new Country(countryName, confirmed, deaths, recovered);
    }

    public static Comparator<CountryData> compareConfirmed = new Comparator<CountryData>() {
        public int compare(CountryData s1, CountryData s2) {
            return (s2.confirmed)-(s1.confirmed);
        };
    };

    public static Comparator<CountryData> compareDeaths = new Comparator<CountryData>() {
        public int compare(CountryData s1, CountryData s2) {
            return (s2.deaths)-(s1.deaths);
        };
    };

    public static Comparator<CountryData> compareRecovered= new Comparator<CountryData>() {
        public int compare(CountryData s1, CountryData s2) {
            return (s2.recovered)-(s1.recovered);
        };
    };
}
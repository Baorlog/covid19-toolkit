package com.example.covid19toolkit.model;

public class Province {
    private String prov;
    private int victimNumber;
    private int deadNumber;
    private int recoverNumber;

    public Province(){}
    public Province(String addr, int num1, int num2, int num3){
        this.prov =addr;
        this.victimNumber=num1;
        this.deadNumber=num2;
        this.recoverNumber=num3;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public int getVictimNumber() {
        return victimNumber;
    }

    public void setVictimNumber(int distance) {
        this.victimNumber = distance;
    }

    public int getDeadNumber() {
        return deadNumber;
    }

    public void setDeadNumber(int deadNumber) {
        this.deadNumber = deadNumber;
    }

    public int getRecoverNumber() {
        return recoverNumber;
    }

    public void setRecoverNumber(int recoverNumber) {
        this.recoverNumber = recoverNumber;
    }
}

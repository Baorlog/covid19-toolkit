package com.example.covid19toolkit.model;

import java.util.Comparator;

public class ProvinceAPI {

    public String provinceID,
            name,
            lastUpdate;

    public int confirmed,
            deaths,
            recovered;

    public float map_lat,
            map_long;

    public ProvinceAPI(String provinceID, String name, int confirmed, int deaths, int recovered, float map_lat, float map_long, String lastUpdate) {
        this.provinceID = provinceID;
        this.name = name;
        this.lastUpdate = lastUpdate;
        this.confirmed = confirmed;
        this.deaths = deaths;
        this.recovered = recovered;
        this.map_lat = map_lat;
        this.map_long = map_long;
    }

    public static Comparator<ProvinceAPI> compareConfirmed = new Comparator<ProvinceAPI>() {
        public int compare(ProvinceAPI s1, ProvinceAPI s2) {
            return (s2.confirmed)-(s1.confirmed);
        };
    };

    public static Comparator<ProvinceAPI> compareDeaths = new Comparator<ProvinceAPI>() {
        public int compare(ProvinceAPI s1, ProvinceAPI s2) {
            return (s2.deaths)-(s1.deaths);
        };
    };

    public static Comparator<ProvinceAPI> compareRecovered= new Comparator<ProvinceAPI>() {
        public int compare(ProvinceAPI s1, ProvinceAPI s2) {
            return (s2.recovered)-(s1.recovered);
        };
    };

    public Province toProvince(){
        return new Province(this.name, this.confirmed, this.deaths, this.recovered);
    }
}

package com.example.covid19toolkit;

import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.covid19toolkit.MainActivity;
import com.example.covid19toolkit.R;

public class WelcomeActivity extends AppCompatActivity {
    String GENERAL_CHANNEL_ID = "General";
    String FOREGROUND_CHANNEL_ID = "Foreground";
    String LOCATION_CHANNEL_ID = "Location";
    final Context context=this;
    View btnGetStarted;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (restorePrefData()) {

            Intent mainActivity = new Intent(this, MainActivity.class );
            startActivity(mainActivity);
            finish();


        }

        setContentView(R.layout.activity_welcome);

        btnGetStarted=findViewById(R.id.btn_start);
        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open main activity
                Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(mainActivity);
                // also we need to save a boolean value to storage so next time when the user run the app
                // we could know that he is already checked the intro screen activity
                // i'm going to use shared preferences to that process
                savePrefsData();
                finish();
            }
        });

        createNotificationChannel(GENERAL_CHANNEL_ID, "General Notification", "Notification about Covid 19 :)");
        createNotificationChannel(FOREGROUND_CHANNEL_ID, "Foreground Notification", "Covid 19 Survival Kit's foreground notification (Keep app running in background");
        createNotificationChannel(LOCATION_CHANNEL_ID, "Location disease Notification", "Keep track of current location and notify about disease status");
    }

    private boolean restorePrefData() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs",MODE_PRIVATE);
        return pref.getBoolean("isIntroOpened",false);
    }
    private void savePrefsData() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isIntroOpened",true);
        editor.apply();


    }

    public void showAppInfo(View v){
        ImageView btnClose;
        LinearLayout bgTransperant;

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_app_info);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        Dialog myDialog;
//        myDialog.setContentView(R.layout.dialog_app_info);
        btnClose = (ImageView) dialog.findViewById(R.id.txtclose);
//        btnClose.setText("X");
        Button btnBack = (Button) dialog.findViewById(R.id.btn_back);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }                                    }
        );

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { dialog.dismiss(); }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent welcomeActivity = new Intent(getApplicationContext(),WelcomeActivity.class);
//                startActivity(welcomeActivity);
                dialog.dismiss();
            }
        });
//        dialog.getWindow().setBackgroundDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void createNotificationChannel(String CHANNEL_ID, String CHANNEL_NAME, String CHANNEL_DESCRIPTION) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = CHANNEL_NAME;
            String description = CHANNEL_DESCRIPTION;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}

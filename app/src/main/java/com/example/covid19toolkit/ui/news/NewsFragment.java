package com.example.covid19toolkit.ui.news;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.covid19toolkit.APIQuery.DataAPI;
import com.example.covid19toolkit.R;
import com.example.covid19toolkit.adapter.AdapterNews;
import com.example.covid19toolkit.model.NewsModel;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class NewsFragment extends Fragment {

    SharedPreferences pref;

    private NewsViewModel slideshowViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private NewsDataBroadcastReceiver newsBroadcastReceiver;
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(NewsViewModel.class);
        view = inflater.inflate(R.layout.fragment_news, container, false);
//        final TextView textView = root.findViewById(R.id.text_news);
        final SwipeRefreshLayout pullToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                pullToRefresh.setRefreshing(false);
            }


        });
        slideshowViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
//                textView.setText(s);
            }
        });
        return view;
    }

    private void refreshData() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (Build.VERSION.SDK_INT >= 26) {
            ft.setReorderingAllowed(false);
        }
        ft.detach(this).attach(this).commit();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //Init
        FetchTopNewsData fetchTopNewsData = new FetchTopNewsData();
        fetchTopNewsData.execute();

        //Updater
        newsBroadcastReceiver = new NewsDataBroadcastReceiver();
        IntentFilter countryFilter = new IntentFilter("Top_News_Data");
        getActivity().registerReceiver(newsBroadcastReceiver, countryFilter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pref = context.getSharedPreferences("TempData", 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(newsBroadcastReceiver);
    }

    private class NewsDataBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try{
                JSONObject json = new JSONObject(intent.getExtras().getString("Data"));
                Iterator<?> keys = json.keys();
                HashMap<String, NewsModel> result = new HashMap<String, NewsModel>();
                while(keys.hasNext())
                {
                    String key = (String)keys.next();
                    Gson gson = new Gson();
                    NewsModel temp = gson.fromJson(json.getJSONObject(key).toString(), NewsModel.class);
                    result.put(key, temp);
                }
                buildNewsData(result);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class FetchTopNewsData extends AsyncTask<Void, Void, HashMap<String,NewsModel>> {

        @Override
        protected HashMap<String,NewsModel> doInBackground(Void... voids) {
            try {
                JSONObject json = new JSONObject(pref.getString("Top_News_Data", "@ERROR:NO_DATA"));
                Iterator<?> keys = json.keys();
                HashMap<String, NewsModel> result = new HashMap<String, NewsModel>();
                while(keys.hasNext())
                {
                    String key = (String)keys.next();
                    Gson gson = new Gson();
                    NewsModel temp = gson.fromJson(json.getJSONObject(key).toString(), NewsModel.class);
                    result.put(key, temp);
                }
                return result;
            } catch (Exception e){
                e.printStackTrace();
                try {
                    JSONObject json = new JSONObject(DataAPI.fetchTopNewsData());
                    Iterator<?> keys = json.keys();
                    HashMap<String, NewsModel> result = new HashMap<String, NewsModel>();
                    while(keys.hasNext())
                    {
                        String key = (String)keys.next();
                        Gson gson = new Gson();
                        NewsModel temp = gson.fromJson(json.getJSONObject(key).toString(), NewsModel.class);
                        result.put(key, temp);
                    }
                    return result;
                } catch (Exception e2){
                    e2.printStackTrace();
                    return null;
                }
            }
        }

        @Override
        protected void onPostExecute(HashMap<String,NewsModel> result) {
            if(result!=null){
                buildNewsData(result);
            }
        }
    }

    private void buildNewsData(HashMap<String, NewsModel> result){
        recyclerView = (RecyclerView) view.findViewById(R.id.rvListNews);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        ArrayList<NewsModel> newsData = new ArrayList<NewsModel>();
        for(String key : result.keySet()){
            newsData.add(result.get(key));
        }
        mAdapter = new AdapterNews(newsData, getContext());
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
    }
}

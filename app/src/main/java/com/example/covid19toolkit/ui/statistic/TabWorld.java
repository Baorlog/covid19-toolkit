package com.example.covid19toolkit.ui.statistic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.covid19toolkit.APIQuery.DataAPI;
import com.example.covid19toolkit.R;
import com.example.covid19toolkit.adapter.AdapterCountry;
import com.example.covid19toolkit.model.Country;
import com.example.covid19toolkit.model.CountryAPI;
import com.example.covid19toolkit.model.CountryData;
import com.example.covid19toolkit.model.SummaryAPI;
import com.example.covid19toolkit.model.TrendsAPI;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class TabWorld extends Fragment {

    SharedPreferences pref;

    public View view;
    private LineChart lineChart;
    private LineData lineData;
    private LineDataSet lineDataVictim;
    private LineDataSet lineDataDead;
    private LineDataSet lineDataRecovered;
    ArrayList<ILineDataSet> dataSets= new ArrayList<>();;
    int colorArray[] = {Color.RED, Color.BLACK,Color.GREEN};
    String[] legendName = {"Bệnh nhân", "Tử vong", "Hồi phục"};
    private TrendsDataBroadcastReceiver trendsBroadcastReceiver;
    private SummaryDataBroadcastReceiver summaryBroadcastReceiver;
    private CountriesDataBroadcastReceiver countriesBroadcastReceiver;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.tab_world, container, false);
        TextView date = (TextView) view.findViewById(R.id.tv_date);
        final SwipeRefreshLayout pullToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                pullToRefresh.setRefreshing(false);
            }
        });
        Date lastUpdate = new Date();
        Locale current = getResources().getConfiguration().locale;
        hideLoadingBar();
        if (current.toString().equalsIgnoreCase("vi")) {
            date.setText(String.format("Ngày %d tháng %d năm %d", lastUpdate.getDate(), lastUpdate.getMonth()+1, lastUpdate.getYear()+1900));
        } else {
            date.setText(String.format(" %d of %s %d", lastUpdate.getDate(), getMonth(lastUpdate.getMonth()), lastUpdate.getYear()+1900));
        }
        return view;
    }

    private void refreshData() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (Build.VERSION.SDK_INT >= 26) {
            ft.setReorderingAllowed(false);
        }
        ft.detach(this).attach(this).commit();
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month+1];
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //Init
        FetchTrendsData fetchTrendsTask = new FetchTrendsData();
        fetchTrendsTask.execute();
        FetchSummaryData fetchSummaryTask = new FetchSummaryData();
        fetchSummaryTask.execute();
        FetchCountriesData fetchCountriesData = new FetchCountriesData();
        fetchCountriesData.execute();
        //Updater
        trendsBroadcastReceiver = new TrendsDataBroadcastReceiver();
        IntentFilter trendsFilter = new IntentFilter("Trends_Data");
        getActivity().registerReceiver(trendsBroadcastReceiver, trendsFilter);
        summaryBroadcastReceiver = new SummaryDataBroadcastReceiver();
        IntentFilter summaryFilter = new IntentFilter("Summary_Data");
        getActivity().registerReceiver(summaryBroadcastReceiver, summaryFilter);
        countriesBroadcastReceiver = new CountriesDataBroadcastReceiver();
        IntentFilter countryFilter = new IntentFilter("Countries_Data");
        getActivity().registerReceiver(countriesBroadcastReceiver, countryFilter);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pref = context.getSharedPreferences("TempData", 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(trendsBroadcastReceiver);
        getActivity().unregisterReceiver(summaryBroadcastReceiver);
        getActivity().unregisterReceiver(countriesBroadcastReceiver);
    }

    private class SummaryDataBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            HashMap<String, CountryData> Countries_Data = new HashMap<String,CountryData>();
            Gson gson = new Gson();
            SummaryAPI result = gson.fromJson(intent.getExtras().getString("Data"), SummaryAPI.class);
            if(result != null) {
                TextView victim_num = (TextView) view.findViewById(R.id.tv_tg_victim_num);
                if(victim_num!=null) {
                    victim_num.setText(String.format("%d (%.2f%%)", result.totalConfirmed,((float)(result.totalConfirmed-result.totalConfirmedLast)*100/(float)result.totalConfirmedLast)));
                }
                TextView dead_num = (TextView) view.findViewById(R.id.tv_tg_dead_num);
                if(dead_num!=null) {
                    dead_num.setText(String.format("%d (%.2f%%)", result.totalDeaths,((float)(result.totalDeaths-result.totalDeathsLast)*100/(float)result.totalDeathsLast)));
                }
                TextView recover_num = (TextView) view.findViewById(R.id.tv_tg_recover_num);
                if(recover_num!=null) {
                    recover_num.setText(String.format("%d (%.2f%%)", result.totalRecovered,((float)(result.totalRecovered-result.totalRecoveredLast)*100/(float)result.totalRecoveredLast)));
                }
            }
        }
    }

    private class TrendsDataBroadcastReceiver extends BroadcastReceiver {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                HashMap<String, CountryData> Countries_Data = new HashMap<String, CountryData>();
                Gson gson = new Gson();
                Type trendsListType = new TypeToken<ArrayList<TrendsAPI>>() {
                }.getType();
                ArrayList<TrendsAPI> result = gson.fromJson(intent.getExtras().getString("Data"), trendsListType);
                ArrayList<Entry> confirmed = new ArrayList<Entry>();
                ArrayList<Entry> deaths = new ArrayList<Entry>();
                ArrayList<Entry> recovered = new ArrayList<Entry>();
                result.sort(TrendsAPI.compareTime);
                int i = 0;
                for (TrendsAPI trend : result) {
                    confirmed.add(new Entry(i, trend.confirmed));
                    deaths.add(new Entry(i, trend.deaths));
                    recovered.add(new Entry(i, trend.recovered));
                    i++;
                }
                buildChart(confirmed,deaths,recovered);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class CountriesDataBroadcastReceiver extends BroadcastReceiver {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onReceive(Context context, Intent intent) {
            try{
                Gson gson = new Gson();
                CountryAPI result = gson.fromJson(intent.getExtras().getString("Data"), CountryAPI.class);
                ArrayList<CountryData> data = new ArrayList<CountryData>(Arrays.asList(result.Items));
                data.sort(CountryData.compareConfirmed);
                buildCountriesData(data);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class FetchTrendsData extends AsyncTask<Void,Void, HashMap<String,ArrayList<Entry>>>{

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected HashMap<String,ArrayList<Entry>> doInBackground(Void... voids) {
            try {
                Gson gson = new Gson();
                Type trendsListType = new TypeToken<ArrayList<TrendsAPI>>(){}.getType();
                ArrayList<TrendsAPI> result = gson.fromJson(pref.getString("Trends_Data","@ERROR:NO_DATA"), trendsListType);
                ArrayList<Entry> confirmed =  new ArrayList<Entry>();
                ArrayList<Entry> deaths =  new ArrayList<Entry>();
                ArrayList<Entry> recovered =  new ArrayList<Entry>();
                result.sort(TrendsAPI.compareTime);
                int i = 0;
                for(TrendsAPI trend : result) {
                    confirmed.add(new Entry(i, trend.confirmed));
                    deaths.add(new Entry(i, trend.deaths));
                    recovered.add(new Entry(i, trend.recovered));
                    i++;
                }
                HashMap<String,ArrayList<Entry>> output =  new HashMap<>();
                output.put("confirmedDataSet", confirmed);
                output.put("deathsDataSet", deaths);
                output.put("recoveredDataSet", recovered);
                return output;
            } catch (Exception e){
                e.printStackTrace();
                try {
                    Gson gson = new Gson();
                    Type trendsListType = new TypeToken<ArrayList<TrendsAPI>>(){}.getType();
                    ArrayList<TrendsAPI> result = gson.fromJson(DataAPI.fetchTrendsData(), trendsListType);
                    ArrayList<Entry> confirmed =  new ArrayList<Entry>();
                    ArrayList<Entry> deaths =  new ArrayList<Entry>();
                    ArrayList<Entry> recovered =  new ArrayList<Entry>();
                    result.sort(TrendsAPI.compareTime);
                    int i = 0;
                    for(TrendsAPI trend : result) {
                        confirmed.add(new Entry(i, trend.confirmed));
                        deaths.add(new Entry(i, trend.deaths));
                        recovered.add(new Entry(i, trend.recovered));
                        i++;
                    }
                    HashMap<String,ArrayList<Entry>> output =  new HashMap<>();
                    output.put("confirmedDataSet", confirmed);
                    output.put("deathsDataSet", deaths);
                    output.put("recoveredDataSet", recovered);
                    return output;
                } catch (Exception e2){
                    e2.printStackTrace();
                    return null;
                }
            }
        }

        @Override
        protected void onPostExecute(HashMap<String, ArrayList<Entry>> result) {
            buildChart(result.get("confirmedDataSet"),result.get("deathsDataSet"),result.get("recoveredDataSet"));
        }
    }

    private class FetchSummaryData extends AsyncTask<Void, Void, SummaryAPI> {

        @Override
        protected SummaryAPI doInBackground(Void... voids) {
            try {
                HashMap<String, CountryData> Countries_Data = new HashMap<String, CountryData>();
                Gson gson = new Gson();
                return gson.fromJson(pref.getString("Summary_Data", "@ERROR:NO_DATA"), SummaryAPI.class);
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    HashMap<String, CountryData> Countries_Data = new HashMap<String, CountryData>();
                    Gson gson = new Gson();
                    return gson.fromJson(DataAPI.fetchSummaryData(), SummaryAPI.class);
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return null;
                }
            }
        }

        @Override
        protected void onPostExecute(SummaryAPI result) {
            if(result != null) {
                TextView victim_num = (TextView) view.findViewById(R.id.tv_tg_victim_num);
                if(victim_num!=null) {
                    victim_num.setText(String.format("%d (%.2f%%)", result.totalConfirmed,((float)(result.totalConfirmed-result.totalConfirmedLast)*100/(float)result.totalConfirmedLast)));
                }
                TextView dead_num = (TextView) view.findViewById(R.id.tv_tg_dead_num);
                if(dead_num!=null) {
                    dead_num.setText(String.format("%d (%.2f%%)", result.totalDeaths,((float)(result.totalDeaths-result.totalDeathsLast)*100/(float)result.totalDeathsLast)));
                }
                TextView recover_num = (TextView) view.findViewById(R.id.tv_tg_recover_num);
                if(recover_num!=null) {
                    recover_num.setText(String.format("%d (%.2f%%)", result.totalRecovered,((float)(result.totalRecovered-result.totalRecoveredLast)*100/(float)result.totalRecoveredLast)));
                }
            }
        }
    }

    private class FetchCountriesData extends AsyncTask<Void, Void, ArrayList<CountryData>>{

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected ArrayList<CountryData> doInBackground(Void... voids) {
            try{
                Gson gson = new Gson();
                CountryAPI result = gson.fromJson(pref.getString("Countries_Data", "@ERROR:NO_DATA"), CountryAPI.class);
                ArrayList<CountryData> data = new ArrayList<CountryData>(Arrays.asList(result.Items));
                data.sort(CountryData.compareConfirmed);
                return data;
            } catch (Exception e){
                e.printStackTrace();
                try{
                    Gson gson = new Gson();
                    CountryAPI result = gson.fromJson(DataAPI.fetchCountriesData(), CountryAPI.class);
                    ArrayList<CountryData> data = new ArrayList<CountryData>(Arrays.asList(result.Items));
                    data.sort(CountryData.compareConfirmed);
                    return data;
                } catch (Exception e2){
                    e2.printStackTrace();
                    return null;
                }
            }
        }

        @Override
        protected void onPostExecute(ArrayList<CountryData> data) {
            if(data!=null) {
                buildCountriesData(data);
            }
        }
    }

    private void buildChart(ArrayList<Entry> confirmed, ArrayList<Entry> deaths, ArrayList<Entry> recovered){
        lineChart = view.findViewById(R.id.chart_tg);
        lineDataVictim = new LineDataSet(confirmed, "Nhiễm bệnh");
        lineDataDead = new LineDataSet(deaths, "Tử vong");
        lineDataRecovered = new LineDataSet(recovered, "Khỏi bệnh");
        dataSets.add(lineDataVictim);
        dataSets.add(lineDataDead);
        dataSets.add(lineDataRecovered);

        //Style chart
        lineChart.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        lineChart.setDrawGridBackground(false);
        //style line
        lineDataVictim.setLineWidth(2);
        lineDataVictim.setColor(Color.RED);
        lineDataVictim.setDrawCircles(false);
//        lineDataVictim.setCircleColor(Color.RED);
        lineDataDead.setLineWidth(2);
        lineDataDead.setColor(Color.BLACK);
        lineDataDead.setDrawCircles(false);
//        lineDataDead.setCircleColor(Color.BLACK);
        lineDataRecovered.setLineWidth(2);
        lineDataRecovered.setColor(Color.GREEN);
//        lineDataRecovered.setCircleColor(Color.GREEN);
        lineDataRecovered.setColor(Color.GREEN);
        lineDataRecovered.setDrawCircles(false);
//        lineDataRecovered.setCircleColor(Color.GREEN);
        //style legend
        Legend legend = lineChart.getLegend();
        legend.setEnabled(true);
        legend.setForm(Legend.LegendForm.LINE);
        legend.setFormSize(30);
        legend.setXEntrySpace(15);
        legend.setFormToTextSpace(10);
        LegendEntry[] lgEntries= new LegendEntry[3];
        for (int i=0; i<lgEntries.length; i++){
            LegendEntry entry = new LegendEntry();
            entry.formColor=colorArray[i];
            entry.label= String.valueOf(legendName[i]);
            lgEntries[i]=entry;
        }
        legend.setCustom(lgEntries);
        Description des = new Description();
        des.setText("Thời gian");
        lineChart.setDescription(des);

        //set chart
        lineData = new LineData(dataSets);
        lineChart.setData(lineData);
        lineChart.invalidate();
    }

    private void buildCountriesData(ArrayList<CountryData> countriesData){

        //recycleview for notifications
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_countries);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        // specify an adapter (see also next example)
        ArrayList<Country> countries = new ArrayList<>();
        for(CountryData temp : countriesData) {
            countries.add(temp.toCountry());
        }
        mAdapter = new AdapterCountry(countries);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
    }
    private void showLoadingBar(){
        ProgressBar circleBar =(ProgressBar) view.findViewById(R.id.loading_bar);
        circleBar.setVisibility(View.VISIBLE);
    }
    private void hideLoadingBar(){
        ProgressBar circleBar =(ProgressBar) view.findViewById(R.id.loading_bar);
        circleBar.setVisibility(View.GONE);
    }
}
package com.example.covid19toolkit.ui.map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covid19toolkit.R;
import com.example.covid19toolkit.adapter.AdapterDangerousArea;
import com.example.covid19toolkit.model.Area;
import com.example.covid19toolkit.model.CountryAPI;
import com.example.covid19toolkit.model.CountryData;
import com.example.covid19toolkit.model.CurrentLocation;
import com.example.covid19toolkit.model.ProvinceAPI;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

public class MapFragment extends Fragment  implements OnMapReadyCallback{
    SharedPreferences pref;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private GoogleMap mMap;
    private View view;
    SupportMapFragment mapFragment;
    protected GeoDataClient mGeoDataClient;
    protected PlaceDetectionClient mPlaceDetectionClient;
    Location currentLocation;
    FusedLocationProviderClient mFusedLocationProviderClient;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION=101; //REQUEST CODE
    private static final int DEFAULT_ZOOM=5;
    boolean mLocationPermissionGranted=false;
    LatLng mDefaultLocation = new LatLng(21.0278, 105.8342);


    public MapFragment(){}
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(this.requireActivity(), null);

        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(this.requireActivity(), null);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_map, container, false);

        mapFragment= (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if(mapFragment == null){
            FragmentManager fm= getFragmentManager();
            FragmentTransaction ft= fm.beginTransaction();
            mapFragment= SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);

        return view;
    }





    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_alert_address);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        // specify an adapter (see also next example)
        ArrayList<Area> addrs = new ArrayList<Area>();
        Area add1=new Area("cau giay", 3);
        addrs.add(add1);
        mAdapter = new AdapterDangerousArea(addrs);
        recyclerView.setAdapter(mAdapter);
    }


    public void onMapReady(GoogleMap googleMap) {
//        MapsInitializer.initialize(getContext());
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
       // Add a marker in VN and move the camera
//        LatLng mDefaultLocation = new LatLng(21.0278, 105.8342);
        updateLocationUI();
        mMap.setInfoWindowAdapter(new PopupAdapter(getLayoutInflater()));
        MarkProvincesLocation markProvincesLocation = new MarkProvincesLocation();
        markProvincesLocation.execute();
        MarkCountriesData markCountriesData = new MarkCountriesData();
        markCountriesData.execute();
    }


    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                getDeviceLocation();
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                currentLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                CurrentLocation currentLocation = new CurrentLocation(getContext());
                Task locationResult = currentLocation.getCurrentLocation();
                locationResult.addOnSuccessListener( new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location!=null) {
                            // Set the map's camera position to the current location of the device.
                            LatLng currentPosition=new LatLng(location.getLatitude(),location.getLongitude());
                            Log.e("current position", currentPosition.toString());
                            Toast.makeText(getContext(),currentPosition.toString(),Toast.LENGTH_SHORT);
                            mMap.addMarker(new MarkerOptions().position(currentPosition).title("I'm here").snippet("Here"));
                            CameraPosition here= CameraPosition.builder().target(currentPosition).zoom(DEFAULT_ZOOM).bearing(0).tilt(45).build();
                            mMap.animateCamera(CameraUpdateFactory.newLatLng(currentPosition));
                            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(here));
                        } else {
                            Log.d("cr loc", "Current location is null. Using defaults.");
//                            Log.e("exc", "Exception: %s", task.getException());
                            mMap.addMarker(new MarkerOptions().position(mDefaultLocation).title("Marker in Vietnam").snippet("Here"));
                            CameraPosition VN= CameraPosition.builder().target(mDefaultLocation).zoom(DEFAULT_ZOOM).bearing(0).tilt(45).build();
                            mMap.animateCamera(CameraUpdateFactory.newLatLng(mDefaultLocation));
                            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(VN));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }

                });

//
            }
        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            updateLocationUI();
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           @NonNull String permissions[],
//                                           @NonNull int[] grantResults) {
//        mLocationPermissionGranted = false;
//        switch (requestCode) {
//            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    mLocationPermissionGranted = true;
//                }
//            }
//        }
//        updateLocationUI();
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pref = context.getSharedPreferences("TempData", 0);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapFragment.onPause();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapFragment.onDestroy();
    }

    private class MarkProvincesLocation extends AsyncTask<Void, Void, ArrayList<ProvinceAPI>> {

        @Override
        protected ArrayList<ProvinceAPI> doInBackground(Void... voids) {
            try{
                Gson gson = new Gson();
                Type provincesListType = new TypeToken<ArrayList<ProvinceAPI>>(){}.getType();
                return gson.fromJson(pref.getString("Provinces_Data","@ERROR:NO_DATA"), provincesListType);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<ProvinceAPI> result) {
            if(result!=null) {
                for(ProvinceAPI provinceData : result){
                    LatLng latLng = new LatLng(provinceData.map_lat,provinceData.map_long);
                    int size = 50+1*provinceData.confirmed;
                    Bitmap markerBitmap = makeDst(size);
                    mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.fromBitmap(markerBitmap))
                            .anchor((float) 0.5, (float) 0.5)
                            .title(provinceData.name)
                            .snippet(String.format("Số ca nhiễm: %d\nSố ca tử vong: %d\nSố ca phục hồi: %d", provinceData.confirmed, provinceData.deaths, provinceData.recovered)));
                }
            }
        }

        protected Bitmap makeDst(int size) {
            Bitmap bm = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bm);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);

            p.setColor(0x66FF7700);
            c.drawOval(new RectF(0, 0, size, size), p);
            return bm;
        }
    }

    private class MarkCountriesData extends AsyncTask<Void, Void, ArrayList<CountryData>>{

        @Override
        protected ArrayList<CountryData> doInBackground(Void... voids) {
            try{
                Gson gson = new Gson();
                CountryAPI result = gson.fromJson(pref.getString("Countries_Data", "@ERROR:NO_DATA"), CountryAPI.class);
                ArrayList<CountryData> data = new ArrayList<CountryData>(Arrays.asList(result.Items));
                return data;
            }
            catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<CountryData> data) {
            if(data!=null) {
                for(CountryData countryData : data){
                    if(countryData.map_lat!=0 && countryData.map_long!=0 && countryData.confirmed!=0 && countryData.countryName!="Vietnam") {
                        LatLng latLng = new LatLng(countryData.map_lat, countryData.map_long);
                        int size = 100 + (int) Math.floor(0.0001 * countryData.confirmed);
                        Bitmap markerBitmap = makeDst(size);
                        mMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .icon(BitmapDescriptorFactory.fromBitmap(markerBitmap))
                                .anchor((float) 0.5, (float) 0.5)
                                .title(countryData.countryName)
                                .snippet(String.format("Số ca nhiễm: %d\nSố ca tử vong: %d\nSố ca phục hồi: %d", countryData.confirmed, countryData.deaths, countryData.recovered)));
                    }
                }
            }
        }

        protected Bitmap makeDst(int size) {
            Bitmap bm = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bm);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);

            p.setColor(0x66FF0000);
            c.drawOval(new RectF(0, 0, size, size), p);
            return bm;
        }
    }
}

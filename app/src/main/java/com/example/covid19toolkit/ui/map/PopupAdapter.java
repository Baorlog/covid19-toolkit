package com.example.covid19toolkit.ui.map;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.covid19toolkit.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class PopupAdapter implements GoogleMap.InfoWindowAdapter {
    private View popup=null;
    private LayoutInflater inflater=null;

    PopupAdapter(LayoutInflater inflater) {
        this.inflater=inflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return(null);
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoContents(Marker marker) {
        if (popup == null) {
            popup=inflater.inflate(R.layout.dialog_disease_area_info, null);
        }

        TextView tv=(TextView)popup.findViewById(R.id.location_name);

        tv.setText(marker.getTitle());
        tv=(TextView)popup.findViewById(R.id.location_data);
        tv.setText(marker.getSnippet());

        return(popup);
    }
}
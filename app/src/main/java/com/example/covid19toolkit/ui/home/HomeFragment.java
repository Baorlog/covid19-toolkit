package com.example.covid19toolkit.ui.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.covid19toolkit.APIQuery.DataAPI;
import com.example.covid19toolkit.R;
import com.example.covid19toolkit.model.CountryData;
import com.example.covid19toolkit.model.SummaryAPI;
import com.google.gson.Gson;

import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class HomeFragment extends Fragment {

    SharedPreferences pref;
    SummaryDataBroadcastReceiver broadcastReceiver;

    private View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.tab_general, container, false);
        final SwipeRefreshLayout pullToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                pullToRefresh.setRefreshing(false);
            }
        });
        TextView date = (TextView) view.findViewById(R.id.tv_date);
        Date lastUpdate = new Date();
        Locale current = getResources().getConfiguration().locale;
        if (current.toString().equalsIgnoreCase("vi")) {
            date.setText(String.format("Ngày %d tháng %d năm %d", lastUpdate.getDate(), lastUpdate.getMonth()+1, lastUpdate.getYear()+1900));

        } else {
            date.setText(String.format(" %d of %s %d", lastUpdate.getDate(), getMonth(lastUpdate.getMonth()), lastUpdate.getYear()+1900));
        }
        return view;
    }
    private void refreshData() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (Build.VERSION.SDK_INT >= 26) {
            ft.setReorderingAllowed(false);
        }
        ft.detach(this).attach(this).commit();
    }
    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month+1];
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        fetchData fetchTask = new fetchData();
        fetchTask.execute();
        broadcastReceiver = new SummaryDataBroadcastReceiver();
        IntentFilter filter = new IntentFilter("Summary_Data");
        getActivity().registerReceiver(broadcastReceiver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pref = context.getSharedPreferences("TempData", 0);
    }

    private class SummaryDataBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            HashMap<String, CountryData> Countries_Data = new HashMap<String,CountryData>();
            Gson gson = new Gson();
            SummaryAPI result = gson.fromJson(intent.getExtras().getString("Data"), SummaryAPI.class);
            if(result != null) {
                TextView victim_num = (TextView) view.findViewById(R.id.tv_victim_num);
                TextView victim_inc = (TextView) view.findViewById(R.id.tv_victim_incr);
                if(victim_num!=null) {
                    victim_num.setText(String.format("%d", result.totalConfirmed));
                }
                if(victim_inc!=null) {
                    victim_inc.setText(String.format("%d (%.2f%%)", result.totalConfirmed-result.totalConfirmedLast,((float)(result.totalConfirmed-result.totalConfirmedLast)*100/(float)result.totalConfirmedLast)));
                }
                TextView dead_num = (TextView) view.findViewById(R.id.tv_dead_num);
                TextView dead_inc = (TextView) view.findViewById(R.id.tv_dead_incr);
                if(dead_num!=null) {
                    dead_num.setText(String.format("%d", result.totalDeaths));
                }
                if(dead_inc!=null) {
                    dead_inc.setText(String.format("%d (%.2f%%)", result.totalDeaths-result.totalDeathsLast,((float)(result.totalDeaths-result.totalDeathsLast)*100/(float)result.totalDeathsLast)));
                }
                TextView recover_num = (TextView) view.findViewById(R.id.tv_recover_num);
                TextView recover_inc = (TextView) view.findViewById(R.id.tv_recover_incr);
                if(recover_num!=null) {
                    recover_num.setText(String.format("%d", result.totalRecovered));
                }
                if(recover_inc!=null) {
                    recover_inc.setText(String.format("%d (%.2f%%)", result.totalRecovered-result.totalRecoveredLast,((float)(result.totalRecovered-result.totalRecoveredLast)*100/(float)result.totalRecoveredLast)));
                }
            }
        }
    }

    private class fetchData extends AsyncTask<Void, Void, SummaryAPI> {

        @Override
        protected SummaryAPI doInBackground(Void... voids) {
            try {
                HashMap<String, CountryData> Countries_Data = new HashMap<String, CountryData>();
                Gson gson = new Gson();
                return gson.fromJson(pref.getString("Summary_Data","@ERROR:NO_DATA"), SummaryAPI.class);
            } catch (Exception e){
                e.printStackTrace();
                try{
                    HashMap<String, CountryData> Countries_Data = new HashMap<String, CountryData>();
                    Gson gson = new Gson();
                    return gson.fromJson(DataAPI.fetchSummaryData(), SummaryAPI.class);
                } catch (Exception e2){
                    e2.printStackTrace();
                    return null;
                }
            }
        }

        @Override
        protected void onPostExecute(SummaryAPI result) {
            if(result != null) {
                TextView victim_num = (TextView) view.findViewById(R.id.tv_victim_num);
                TextView victim_inc = (TextView) view.findViewById(R.id.tv_victim_incr);
                if(victim_num!=null) {
                    victim_num.setText(String.format("%d", result.totalConfirmed));
                }
                if(victim_inc!=null) {
                    victim_inc.setText(String.format("%d (%.2f%%)", result.totalConfirmed-result.totalConfirmedLast,((float)(result.totalConfirmed-result.totalConfirmedLast)*100/(float)result.totalConfirmedLast)));
                }
                TextView dead_num = (TextView) view.findViewById(R.id.tv_dead_num);
                TextView dead_inc = (TextView) view.findViewById(R.id.tv_dead_incr);
                if(dead_num!=null) {
                    dead_num.setText(String.format("%d", result.totalDeaths));
                }
                if(dead_inc!=null) {
                    dead_inc.setText(String.format("%d (%.2f%%)", result.totalDeaths-result.totalDeathsLast,((float)(result.totalDeaths-result.totalDeathsLast)*100/(float)result.totalDeathsLast)));
                }
                TextView recover_num = (TextView) view.findViewById(R.id.tv_recover_num);
                TextView recover_inc = (TextView) view.findViewById(R.id.tv_recover_incr);
                if(recover_num!=null) {
                    recover_num.setText(String.format("%d", result.totalRecovered));
                }
                if(recover_inc!=null) {
                    recover_inc.setText(String.format("%d (%.2f%%)", result.totalRecovered-result.totalRecoveredLast,((float)(result.totalRecovered-result.totalRecoveredLast)*100/(float)result.totalRecoveredLast)));
                }
            }
        }
    }
}

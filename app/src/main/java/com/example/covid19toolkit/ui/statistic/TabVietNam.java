package com.example.covid19toolkit.ui.statistic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.covid19toolkit.APIQuery.DataAPI;
import com.example.covid19toolkit.R;
import com.example.covid19toolkit.adapter.AdapterProvince;
import com.example.covid19toolkit.model.CountryAPI;
import com.example.covid19toolkit.model.CountryData;
import com.example.covid19toolkit.model.Province;
import com.example.covid19toolkit.model.ProvinceAPI;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class TabVietNam extends Fragment {

    SharedPreferences pref;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private CountriesDataBroadcastReceiver VietNamBroadcastReceiver;
    private ProvincesDataBroadcastReceiver provincesBroadcastReceiver;

    private View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.tab_vietnam, container, false);
        TextView date = (TextView) view.findViewById(R.id.tv_date);
        final SwipeRefreshLayout pullToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                pullToRefresh.setRefreshing(false);
            }
        });
        Date lastUpdate = new Date();
        Locale current = getResources().getConfiguration().locale;
        if (current.toString().equalsIgnoreCase("vi")) {
            date.setText(String.format("Ngày %d tháng %d năm %d", lastUpdate.getDate(), lastUpdate.getMonth()+1, lastUpdate.getYear()+1900));

        } else {
            date.setText(String.format(" %d of %s %d", lastUpdate.getDate(), getMonth(lastUpdate.getMonth()), lastUpdate.getYear()+1900));
        }
        return view;
    }

    private void refreshData() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (Build.VERSION.SDK_INT >= 26) {
            ft.setReorderingAllowed(false);
        }
        ft.detach(this).attach(this).commit();
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month+1];
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        FetchCountriesData fetchCountryTask = new FetchCountriesData();
        fetchCountryTask.execute();
        FetchProvincesData fetchProvincesTask = new FetchProvincesData();
        fetchProvincesTask.execute();
        VietNamBroadcastReceiver = new CountriesDataBroadcastReceiver();
        IntentFilter countryFilter = new IntentFilter("Countries_Data");
        getActivity().registerReceiver(VietNamBroadcastReceiver, countryFilter);
        provincesBroadcastReceiver = new ProvincesDataBroadcastReceiver();
        IntentFilter provincesFilter = new IntentFilter("Provinces_Data");
        getActivity().registerReceiver(provincesBroadcastReceiver, provincesFilter);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pref = context.getSharedPreferences("TempData", 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(VietNamBroadcastReceiver);
        getActivity().unregisterReceiver(provincesBroadcastReceiver);
    }

    private class CountriesDataBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try{
                HashMap<String, CountryData> Countries_Data = new HashMap<String,CountryData>();
                Gson gson = new Gson();
                CountryAPI result = gson.fromJson(intent.getExtras().getString("Data"), CountryAPI.class);
                if(result.Items!=null){
                    for(CountryData country : result.Items){
                        Countries_Data.put(country.countryName, country);
                    }
                }
                TextView victim_num = (TextView) view.findViewById(R.id.tv_vn_victim_num);
                if(victim_num!=null) {
                    victim_num.setText(String.valueOf(Countries_Data.get("Vietnam").confirmed));
                }
                TextView dead_num = (TextView) view.findViewById(R.id.tv_vn_dead_num);
                if(dead_num!=null) {
                    dead_num.setText(String.valueOf(Countries_Data.get("Vietnam").deaths));
                }
                TextView recover_num = (TextView) view.findViewById(R.id.tv_vn_recover_num);
                if(recover_num!=null) {
                    recover_num.setText(String.valueOf(Countries_Data.get("Vietnam").recovered));
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class ProvincesDataBroadcastReceiver extends BroadcastReceiver {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Gson gson = new Gson();
                Type provincesListType = new TypeToken<ArrayList<ProvinceAPI>>(){}.getType();
                ArrayList<ProvinceAPI> result = gson.fromJson(intent.getExtras().getString("Data"), provincesListType);
                result.sort(ProvinceAPI.compareConfirmed);
                buildRecyclerView(result);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class FetchCountriesData extends AsyncTask<Void, Void, CountryAPI> {

        @Override
        protected CountryAPI doInBackground(Void... voids) {
            try {
                Gson gson = new Gson();
                return gson.fromJson(pref.getString("Countries_Data","@ERROR:NO_DATA"), CountryAPI.class);
            } catch (Exception e){
                e.printStackTrace();
                try {
                    Gson gson = new Gson();
                    return gson.fromJson(DataAPI.fetchCountriesData(), CountryAPI.class);
                } catch (Exception e2){
                    e2.printStackTrace();
                    return null;
                }
            }
        }

        @Override
        protected void onPostExecute(CountryAPI result) {
            HashMap<String, CountryData> Countries_Data = new HashMap<String,CountryData>();
            if(result!= null && result.Items!=null){
                for(CountryData country : result.Items){
                    Countries_Data.put(country.countryName, country);
                }
            }
            TextView victim_num = (TextView) view.findViewById(R.id.tv_vn_victim_num);
            if(victim_num!=null) {
                victim_num.setText(String.valueOf(Countries_Data.get("Vietnam").confirmed));
            }
            TextView dead_num = (TextView) view.findViewById(R.id.tv_vn_dead_num);
            if(dead_num!=null) {
                dead_num.setText(String.valueOf(Countries_Data.get("Vietnam").deaths));
            }
            TextView recover_num = (TextView) view.findViewById(R.id.tv_vn_recover_num);
            if(recover_num!=null) {
                recover_num.setText(String.valueOf(Countries_Data.get("Vietnam").recovered));
            }
        }
    }

    private class FetchProvincesData extends AsyncTask<Void, Void, ArrayList<ProvinceAPI>> {

        @Override
        protected ArrayList<ProvinceAPI> doInBackground(Void... voids) {
            try{
                Gson gson = new Gson();
                Type provincesListType = new TypeToken<ArrayList<ProvinceAPI>>(){}.getType();
                return gson.fromJson(pref.getString("Provinces_Data","@ERROR:NO_DATA"), provincesListType);
            }catch (Exception e){
                e.printStackTrace();
                try{
                    Gson gson = new Gson();
                    Type provincesListType = new TypeToken<ArrayList<ProvinceAPI>>(){}.getType();
                    return gson.fromJson(DataAPI.fetchProvincesData(), provincesListType);
                }catch (Exception e2){
                    e2.printStackTrace();
                    return null;
                }
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(ArrayList<ProvinceAPI> result) {
            if(result!=null) {
                result.sort(ProvinceAPI.compareConfirmed);
                buildRecyclerView(result);
            }
        }
    }

    private void buildRecyclerView(ArrayList<ProvinceAPI> result){
        //recycleview for notifications
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_provinces);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        // specify an adapter (see also next example)
        ArrayList<Province> provinces = new ArrayList<Province>();
        for(ProvinceAPI temp : result){
            provinces.add(temp.toProvince());
        }
        mAdapter = new AdapterProvince(provinces);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
    }
}



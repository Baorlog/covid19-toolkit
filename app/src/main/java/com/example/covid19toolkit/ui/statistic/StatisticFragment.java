package com.example.covid19toolkit.ui.statistic;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.covid19toolkit.R;
import com.example.covid19toolkit.adapter.ViewPagerAdapter;
import com.example.covid19toolkit.ui.news.NewsViewModel;
import com.google.android.material.tabs.TabLayout;

public class StatisticFragment extends Fragment   {

//    private NewsViewModel slideshowViewModel;


//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
////        tabView();
//
//    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_statistics, container, false);

        return root;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
        TabLayout tabLayout =
                (TabLayout) view.findViewById(R.id.tab_layout);


        final ViewPager viewPager =
                (ViewPager) view.findViewById(R.id.pager);
        final PagerAdapter adapter = new ViewPagerAdapter
                (this.getContext(), getChildFragmentManager());
        viewPager.setAdapter(adapter);

//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));


        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}


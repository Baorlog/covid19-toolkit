package com.example.covid19toolkit.adapter;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.covid19toolkit.R;
import com.example.covid19toolkit.ui.statistic.TabVietNam;
import com.example.covid19toolkit.ui.statistic.TabWorld;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final Context mContext;


    public ViewPagerAdapter(Context context,FragmentManager fm){
        super(fm);
        this.mContext=context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                TabVietNam tab1 = new TabVietNam();
                return tab1;
            case 1:
                TabWorld tab2 = new TabWorld();
                return tab2;
            default: return null;
        }
//        if (position==0){
//            fragment= new TabVietNam();}
//        else if (position==1){
//            fragment= new TabWorld();
//        }
//        return fragment;

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position){
        switch (position){
//            case 0: return  "TỔNG QUAN";

            case 0: return mContext.getResources().getString(R.string.tab_text_2);
            case 1: return mContext.getResources().getString(R.string.tab_text_3);
            default: return null;
        }

    }
}

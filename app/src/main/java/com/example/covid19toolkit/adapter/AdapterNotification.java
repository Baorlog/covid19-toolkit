package com.example.covid19toolkit.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covid19toolkit.R;
import com.example.covid19toolkit.model.InAppNotification;
import com.google.gson.Gson;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class AdapterNotification extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<InAppNotification> notis;
    private Context context;
    public static final int TYPE1 = 0;
    public static final int TYPE2 = 1;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout v =null;
        switch (viewType){
            case TYPE1:
                v = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_unread_notification, parent, false);
                return new ReadViewHolder(v);
            case TYPE2:
                v = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_read_notification, parent, false);
                return new UnReadViewHolder(v);
            default:break;
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (notis.get(position).getRead()==false)
            return TYPE1;
        else
            return TYPE2;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final InAppNotification noti;
        noti= notis.get(position);
        switch (getItemViewType(position)){
            case TYPE1:{
                ReadViewHolder readViewHolder = (ReadViewHolder) holder;
                readViewHolder.tvTitle.setText(String.valueOf(noti.getTitle()));
                readViewHolder.tvTime.setText(noti.compareNow());
                readViewHolder.tvContent.setText(String.valueOf(noti.getContent()));
                setImage(readViewHolder.tvImage, noti.getImageURL());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onClick(View view) {
                        if(noti.getTargetURL()!=null && noti.getTargetURL().length()!=0) {
                            switch(noti.getTargetURL()){
                                case "@VIETNAM":
                                    break;
                                case "@WORLD":
                                    break;
                                case "@LOCATION":
                                    break;
                                default:
                                    Intent notificationIntent = new Intent(Intent.ACTION_VIEW);
                                    notificationIntent.setData(Uri.parse(noti.getTargetURL()));
                                    context.startActivity(notificationIntent);
                            }

                            SharedPreferences notiPref = context.getSharedPreferences("NotiData", 0);
                            SharedPreferences.Editor notiEditor = notiPref.edit();
                            try {
                                String inAppID = noti.getId();
                                Gson gson = new Gson();
                                InAppNotification inAppNotification = gson.fromJson(notiPref.getString(inAppID, "ERROR:NOTI_NOT_FOUND"), InAppNotification.class);
                                inAppNotification.read();
                                notiEditor.putString(inAppID, gson.toJson(inAppNotification));
                                notiEditor.commit();
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                            notiEditor.commit();
                            notis.clear();
                            HashMap<String, String> notiList = (HashMap<String, String>) notiPref.getAll();
                            for(String inAppID : notiList.keySet()){
                                notis.add(new Gson().fromJson(notiList.get(inAppID), InAppNotification.class));
                            }
                            notis.sort(InAppNotification.compareTime);
                            notifyDataSetChanged();
                        }
                    }
                });
                break;
            }
            case TYPE2:{
                UnReadViewHolder unReadViewHolder= (UnReadViewHolder) holder;
                unReadViewHolder.tvTitle.setText(String.valueOf(noti.getTitle()));
                unReadViewHolder.tvTime.setText(noti.compareNow());
                unReadViewHolder.tvContent.setText(String.valueOf(noti.getContent()));
                setImage(unReadViewHolder.tvImage, noti.getImageURL());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch(noti.getTargetURL()){
                            case "@VIETNAM":
                                break;
                            case "@WORLD":
                                break;
                            case "@LOCATION":
                                break;
                            default:
                                Intent notificationIntent = new Intent(Intent.ACTION_VIEW);
                                notificationIntent.setData(Uri.parse(noti.getTargetURL()));
                                context.startActivity(notificationIntent);
                        }
                    }
                });
                break;

            }
        }
    }

    @Override
    public int getItemCount() {
        return notis.size();
    }

    public static  class UnReadViewHolder extends RecyclerView.ViewHolder{
        public Boolean isRead=false;
        public TextView tvTitle;
        public TextView tvTime;
        public TextView tvContent;
        public ImageView tvImage;
        public UnReadViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvTime = (TextView) itemView.findViewById(R.id.tv_received_time);
            tvContent = (TextView) itemView.findViewById(R.id.tv_noti_content);
            tvImage= (ImageView) itemView.findViewById(R.id.iv_image);
        }
    }
    public static  class ReadViewHolder extends RecyclerView.ViewHolder{
        public Boolean isRead=true;
        public TextView tvTitle;
        public TextView tvTime;
        public TextView tvContent;
        public ImageView tvImage;
        public ReadViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvTime = (TextView) itemView.findViewById(R.id.tv_received_time);
            tvContent = (TextView) itemView.findViewById(R.id.tv_noti_content);
            tvImage= (ImageView) itemView.findViewById(R.id.iv_image);
        }
    }


    public AdapterNotification(ArrayList<InAppNotification> myDataset, Context context) {
        this.context = context;
        notis = myDataset;
    }

    protected void setImage(ImageView imageView, String url){
        if(url!=null && url.length()>0){
            switch (url) {
                case "@WORLD":
                    imageView.setImageDrawable(ResourcesCompat.getDrawable(imageView.getResources(), R.drawable.earth, null));
                    break;
                case "@VIETNAM":
                    imageView.setImageDrawable(ResourcesCompat.getDrawable(imageView.getResources(), R.drawable.vietnam, null));
                    break;
                case "@LOCATION":
                    imageView.setImageDrawable(ResourcesCompat.getDrawable(imageView.getResources(), R.drawable.map_marker, null));
                    break;
                default:
                    DownloadImageTask downloadImageTask = new DownloadImageTask(imageView);
                    downloadImageTask.execute(url);
            }
        }
    }

    protected class DownloadImageTask extends AsyncTask<String, Void, Drawable> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Drawable doInBackground(String... urls) {
            try {
                InputStream is = (InputStream) new URL(urls[0]).getContent();
                Drawable d = Drawable.createFromStream(is, "ImageData");
                return d;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Drawable result) {
            if(result!=null)
                bmImage.setImageDrawable(result);
        }
    }
}

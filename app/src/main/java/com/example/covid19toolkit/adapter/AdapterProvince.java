package com.example.covid19toolkit.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covid19toolkit.R;

import java.util.ArrayList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.example.covid19toolkit.R;
import com.example.covid19toolkit.model.Province;

import java.util.ArrayList;

    public class AdapterProvince extends Adapter<com.example.covid19toolkit.adapter.AdapterProvince.MyViewHolder> {
        private ArrayList<Province> provinces;

        @NonNull
        @Override
        public com.example.covid19toolkit.adapter.AdapterProvince.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // create a new view
            LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_province, parent, false);

            com.example.covid19toolkit.adapter.AdapterProvince.MyViewHolder vh = new com.example.covid19toolkit.adapter.AdapterProvince.MyViewHolder(v);
            return vh;
        }



        @Override
        public void onBindViewHolder(@NonNull com.example.covid19toolkit.adapter.AdapterProvince.MyViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            Province province;
            province= provinces.get(position);
            holder.tvProvince.setText(province.getProv());
            holder.tvVictimNumber.setText(String.valueOf(province.getVictimNumber()) );
            holder.tvDeadNumber.setText(String.valueOf(province.getDeadNumber()) );
            holder.tvRecoveredNumber.setText(String.valueOf(province.getRecoverNumber()) );
        }

        @Override
        public int getItemCount() {
            return provinces.size();
        }

        public static  class MyViewHolder  extends RecyclerView.ViewHolder{
            public TextView tvProvince;
            public TextView tvVictimNumber;
            public TextView tvDeadNumber;
            public TextView tvRecoveredNumber;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                tvProvince = (TextView) itemView.findViewById(R.id.tv_province_name);
                tvVictimNumber = (TextView) itemView.findViewById(R.id.tv_victim_prov);
                tvDeadNumber = (TextView) itemView.findViewById(R.id.tv_dead_prov);
                tvRecoveredNumber = (TextView) itemView.findViewById(R.id.tv_recover_prov);
            }
        }

        public AdapterProvince(ArrayList<Province> myDataset) {
            provinces = myDataset;
        }
    }




package com.example.covid19toolkit.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covid19toolkit.R;
import com.example.covid19toolkit.model.NewsModel;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class AdapterNews extends RecyclerView.Adapter<AdapterNews.MyViewHolder> {

    private Context context;
    private ArrayList<NewsModel> news;

    public AdapterNews(ArrayList<NewsModel> news, Context context){
        this.news = news;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final NewsModel newsModel;
        newsModel = news.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(newsModel.getUrl() != null && newsModel.getUrl().length()!=0) {
                    Intent notificationIntent = new Intent(Intent.ACTION_VIEW);
                    notificationIntent.setData(Uri.parse(newsModel.getUrl()));
                    context.startActivity(notificationIntent);
                }
            }
        });
        DownloadImageTask imageTask = new DownloadImageTask(holder.imgNews);
        imageTask.execute(newsModel.getPicture());
        holder.tvTitle.setText(String.valueOf(newsModel.getTitle()));
        holder.tvDescription.setText(String.valueOf(newsModel.getContent()));
        holder.tvAuthor.setText(String.valueOf(newsModel.getAuthor()));
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgNews;
        public TextView tvTitle;
        public TextView tvDescription;
        public TextView tvAuthor;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgNews = (ImageView) itemView.findViewById(R.id.imgNews);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            tvAuthor = (TextView) itemView.findViewById(R.id.tvAuthor);

        }
    }

    protected class DownloadImageTask extends AsyncTask<String, Void, Drawable> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Drawable doInBackground(String... urls) {
            try {
                InputStream is = (InputStream) new URL(urls[0]).getContent();
                Drawable d = Drawable.createFromStream(is, "ImageData");
                return d;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Drawable result) {
            if(result!=null)
                bmImage.setImageDrawable(result);
        }
    }
}

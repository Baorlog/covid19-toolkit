package com.example.covid19toolkit.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.covid19toolkit.R;
import com.example.covid19toolkit.model.Area;

import java.util.ArrayList;

public class AdapterDangerousArea extends RecyclerView.Adapter<AdapterDangerousArea.MyViewHolder> {
    private ArrayList<Area> areas;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_danger_address, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Area area;
        area= areas.get(position);
        holder.tvAddress.setText(area.getAddr());
        holder.tvVictimNumber.setText(String.valueOf(area.getVictimNumber()) );
    }

    @Override
    public int getItemCount() {
        return areas.size();
    }

    public static  class MyViewHolder  extends RecyclerView.ViewHolder{
        public TextView tvAddress;
        public TextView tvVictimNumber;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAddress = (TextView) itemView.findViewById(R.id.tv_address);
            tvVictimNumber = (TextView) itemView.findViewById(R.id.tv_victim_number);
        }
    }

    public AdapterDangerousArea(ArrayList<Area> myDataset) {
        areas = myDataset;
    }
}

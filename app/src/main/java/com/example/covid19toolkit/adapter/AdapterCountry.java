package com.example.covid19toolkit.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.example.covid19toolkit.R;
import com.example.covid19toolkit.model.Country;

import java.util.ArrayList;

public class AdapterCountry extends Adapter<AdapterCountry.MyViewHolder> {
    private ArrayList<Country> countries;

    @NonNull
    @Override
    public AdapterCountry.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_country, parent, false);

        AdapterCountry.MyViewHolder vh = new AdapterCountry.MyViewHolder(v);
        return vh;
    }



    @Override
    public void onBindViewHolder(@NonNull AdapterCountry.MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Country country;
        country= countries.get(position);
        holder.tvCountry.setText(country.getCountry());
        holder.tvVictimNumber.setText(String.valueOf(country.getVictimNumber()) );
        holder.tvDeadNumber.setText(String.valueOf(country.getDeadNumber()) );
        holder.tvRecoveredNumber.setText(String.valueOf(country.getRecoverNumber()) );
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public static  class MyViewHolder  extends RecyclerView.ViewHolder{
        public TextView tvCountry;
        public TextView tvVictimNumber;
        public TextView tvDeadNumber;
        public TextView tvRecoveredNumber;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCountry = (TextView) itemView.findViewById(R.id.tv_country_name);
            tvVictimNumber = (TextView) itemView.findViewById(R.id.tv_victim_country);
            tvDeadNumber = (TextView) itemView.findViewById(R.id.tv_dead_country);
            tvRecoveredNumber = (TextView) itemView.findViewById(R.id.tv_recover_country);
        }
    }

    public AdapterCountry(ArrayList<Country> myDataset) {
        countries = myDataset;
    }
}



